# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import models, fields, api,_

class ProductTemplate(models.Model):
    _inherit="product.template"
    
    not_refunded=fields.Boolean('Non Returnable')