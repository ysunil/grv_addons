from odoo import api, fields, models
import datetime

class PosOrder(models.Model):

    _inherit = "pos.order"

    refund_type = fields.Selection([('no','No'),('partial','Partial'),('full','Full')],string='Refuned',default='no', readonly=1)
    get_subtotal_in_words=fields.Char()
    @api.model
    def get_details(self, ref):
        order_id = self.env['pos.order'].sudo().search([('pos_reference', '=', ref)], limit=1)
        return order_id.ids
    
    
    @api.model
    def get_orderlines(self, ref):
        discount = 0
        result = []
        order_id = self.search([('pos_reference', '=', ref)], limit=1)
        lines = self.env['pos.order.line'].search([('order_id', '=', order_id.id)])
        payments = self.env['account.bank.statement.line'].search([('pos_statement_id', '=', order_id.id)])
        payment_lines = []
        change = 0
        for i in payments:
            if i.amount > 0:
                temp = {
                    'amount': i.amount,
                    'name': i.journal_id.name
                }
                payment_lines.append(temp)
            else:
                change += i.amount
        for line in lines:
            new_vals = {
                'product_id': line.product_id.name,
                'qty': line.qty,
                'price_unit': line.price_unit,
                'discount': line.discount,
                }
            discount += (line.price_unit * line.qty * line.discount) / 100
            result.append(new_vals)

        return [result, discount, payment_lines, change]

    def create(self, vals):
        parent_id=False
        if 'parent_id' in vals:
            orders = self.search([('id', '=', vals['parent_id'])])
            parent_id=orders.id
            total_qty=sum(line.qty for line in orders.lines)
            return_qty=sum(line[2]['qty'] for line in vals.get('lines'))
            if return_qty<total_qty:
                is_refunded='partial'
            elif return_qty==total_qty:
                is_refunded='full'
            if 'old_lines' in vals:
                old_lines=vals.get('old_lines')
                for line in orders.lines.filtered(lambda l:l.id in [int(l) for l in old_lines]):
                    line.write({'refund_qty':line.refund_qty+float(old_lines[str(line.id)])})
            vals.pop('parent_id');
            vals.pop('old_lines');
        new_order=super(PosOrder, self).create(vals)
        new_order.parent_id=parent_id
        if  new_order.parent_id:
            new_order.parent_id.write({'refund_type': is_refunded,})
        return new_order

    @api.model
    def _order_fields(self, ui_order): #v10
        vals = super(PosOrder, self)._order_fields(ui_order)
        total_amount=ui_order['amount_total']
        vals['get_subtotal_in_words']=ui_order['get_subtotal_in_words']
        if 'is_refund' in ui_order:
            vals['is_refund'] = True
            vals['source'] = ui_order['source']
            vals['parent_id'] = ui_order['parent_id']
            vals['old_lines'] = ui_order['old_lines']
            vals['is_refunded'] = ui_order['is_refunded']
        return vals

class PosOrderLine(models.Model):

    _inherit = "pos.order.line"

    refund_qty = fields.Float('Refund/Return Qty')
    mrp_product = fields.Float(related='product_id.mrp_product',store=True)
    not_refundable = fields.Boolean('Not Refundable')
