odoo.define('pos_refund_order.refund_extend', function(require) {
"use strict"
var core = require('web.core');
var screens = require('point_of_sale.screens');
var ScreenWidget = screens.ScreenWidget;
var pos_refund_order= require('pos_refund_order');
var chrome = require('point_of_sale.chrome');
var rpc = require('web.rpc');
var gui = require('point_of_sale.gui');
var QWeb = core.qweb;
var _t = core._t;
var db = require('point_of_sale.DB');

var pos_model = require('point_of_sale.models');
var _modelproto = pos_model.PosModel.prototype;
var models = _modelproto.models;

db.include({
     add_orders: function(orders) {
            var updated_count = 0;
            if(!orders instanceof Array){
                orders = [orders];
            }
            this.order_by_id={};
            this.orders_sorted=[];
            for(var i = 0, len = orders.length; i < len; i++){
                var order = orders[i];
                 if (order.refund_type!=='full'){
                    this.order_by_code_reference[order.pos_reference] = order;
                    this.orders_sorted.push(order);
                    this.order_by_id[order.id] = order;
                    this.order_search_string += this._order_search_string(order);
                    updated_count += 1;
                    }
            }
            return updated_count;
        },
        add_order_lines: function(lines) {
            if (!lines instanceof Array) {
                lines = [lines];
            }
            var count_up = 0;
            this.line_by_id=[];
            this.line_by_pos_reference={};
            for(var i = 0, len = lines.length; i < len; i++){
                var line = lines[i];
                this.line_by_id[line.id] = line;
                count_up += 1
                if (!this.line_by_pos_reference[line.pos_reference]) {
                    this.line_by_pos_reference[line.pos_reference] = []
                    this.line_by_pos_reference[line.pos_reference].push(line);
                } else {
                    this.line_by_pos_reference[line.pos_reference].push(line);
                }
            }
        },
 })
 
for (var i = 0; i < models.length; i++) {
    var model = models[i];
    if (model.model === 'pos.order') {
        model.domain =  function(self){
            return [['state', '!=', 'draft'], ['state', '!=', 'cancel'], ['is_refund', '=', false], ['refund_type', '!=','full']];
        };
    };
}

//load new field 'refund_qty'
pos_model.load_fields("pos.order",["refund_type", "amount_tax",'get_subtotal_in_words']);
pos_model.load_fields("pos.order.line",[ "refund_qty","price_subtotal",'not_refundable']);
pos_model.load_fields("product.product",["not_refunded","mrp_product","list_price"]);


screens.ProductScreenWidget.include({
    show: function(reset){
        this._super();
        var orders = this.pos.db.get_orders_sorted();
        var refund_screen=new pos_refund_order.RefundScreenWidget(this);
        refund_screen.reload_orders();
        
        }
})
pos_refund_order.RefundScreenWidget.include({
    init: function(parent, options){
        this._super(parent, options);
        this.refund_order_cache = new screens.DomCache();
    },
    start: function () {
            var self = this;
            this._super();
            this.reload_orders();
        },
    show: function () {
            var self = this;
            this.renderElement();
            var orders = this.pos.db.get_orders_sorted();
            this.render_list(orders);
            this.reload_orders();
            this._super();
            this.$('.back').click(function(){
                self.gui.show_screen('products');
            });
        },
	reprint_ask_password: function(){
        var self=this;
        var user=self.pos.get_cashier();
        if (user.pos_security_pin==false){
             this.gui.show_popup('error',{
                'title': core._t('Ask Password For Manager !'),
                
            });
        }else{
            return this.gui.ask_password(user.pos_security_pin).then(function(){
            self.made_reprint_order();
        });
        }
    },
    made_reprint_order: function() {
        var self=this;
        var order=this.order_selected
        var lines = [];
        var payments = [];
        var discount = 0;
        self.pos.reprint_order=this.order_selected
        var order=rpc.query({model :'pos.order',method: 'get_orderlines',args: [order.pos_reference],}).then(function(result){
                lines = result[0];
                payments = result[2];
                discount = result[1];
                self.pos.reprint_result=result;
               self.gui.show_screen('order_reprint');
            });
    },
    reload_orders: function(){
        var self = this;
        return this.pos.load_new_orders().then(function(){
            var orders = self.pos.db.get_orders_sorted()
            var pos_references = [];
            for (var i = 0, len = orders.length; i < len; i++) {
                    pos_references.push(orders[i].pos_reference);
            }
            var domain=[['pos_reference', 'in', pos_references]];
            return self.pos.load_new_order_lines(domain).then(function(){;
            self.render_list(self.pos.db.get_orders_sorted())});
        })
        ;
    },
    made_refund_order: function() {
        var self=this;
        var order=this.order_selected
        self.gui.show_popup('refund-line-info-input');
    },
    
    line_select: function(event, element, code){
        var order = this.pos.db.get_order_by_code_reference(code);
        var contents = this.$('.order-details-contents');
        contents.empty();
        this.order_selected = order;
        this.pos.order_selected = order;
        this.lines = this.pos.db.get_line_by_pos_reference(order.pos_reference);
        var all_element_actived = this.$('.highlight');
        var highlight =  element.hasClass('highlight');
        if (all_element_actived.length == 0){
            element.addClass('highlight');

        } else {
            this.$('.order-list-contents .highlight').removeClass('highlight');
            element.addClass('highlight');

        }
        contents.append($(QWeb.render('OrderInformationWidget',{ widget: this, order: order})));
//        $('.button.print').css('height', '400px');
        this.show_refund_button();
        this.show_reprit_button();
    },
    
    show_reprit_button: function(){
            var $button = this.$('.reprint');
            $button.removeClass('oe_hidden');
        },
    render_list: function(orders){
        var contents = this.$el[0].querySelector('.order-list-contents');
        contents.innerHTML = "";
         var order= null;
        for(var i = 0, len = Math.min(orders.length,1000); i < len; i++){
            var order    = orders[i];
            var orderline = this.refund_order_cache.get_node(order.id);
            var orderline_html = QWeb.render('RecoverOrders',{widget: this, order: order});
            var orderline = document.createElement('tbody');
            orderline.innerHTML = orderline_html;
            orderline = orderline.childNodes[1];
            contents.appendChild(orderline);
            this.refund_order_cache.cache_node(order.id,orderline);
            
        }
        this.reload_orders()
    },
    renderElement: function () {
        this._super(this);
        var self = this;
        this.reload_orders();
        this.$('.back').click(function(){
                self.gui.show_screen('products');
            });
        this.$('.made_refund_order').click(function(){
                self.made_refund_order();
            });
        this.$('.reprint').click(function(){
                self.reprint_ask_password()
            });
    },
    close: function(){
        this._super();
    },
});
var _super_order = pos_model.Order.prototype;
pos_model.Order = pos_model.Order.extend({
    export_as_JSON: function () {
        var json
        return _.extend(_super_order.export_as_JSON.apply(this, arguments), {
            is_refunded:this.refund_type,
            old_lines:this.old_lines,
            get_subtotal_in_words : this.get_subtotal_in_words(),
        });
    },
    get_subtotal_in_words:function(){
        var self=this
        var total=0.00
        var total_amount= this.get_total_with_tax()
        if (total_amount<0){
            var total = Math.abs(total_amount)
        }else{
             var total=total_amount
        }
        var amt_txt=self.number2text(total)
        return amt_txt
    },
    number2text:function (value) {
        var self=this
        var fraction = Math.round(self.frac(value)*100);
        var f_text  = "";
        if(fraction > 0) {
            f_text = "And "+self.convert_number(fraction)+" Pasie";
        }
        return self.convert_number(value)+" Rupees "+f_text+" Only";
    },
    frac:function (f) {
        return f % 1;
    },
    convert_number:function (number){
        if ((number < 0) || (number > 999999999)) { 
            return "NUMBER OUT OF RANGE!";
        }
        var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six","Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen","Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen","Nineteen"); 
        var tens = Array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty","Seventy", "Eighty", "Ninety"); 

        var Gn = Math.floor(number / 10000000);  /* Crore */ 
        number -= Gn * 10000000; 
        var kn = Math.floor(number / 100000);     /* lakhs */ 
        number -= kn * 100000; 
        var Hn = Math.floor(number / 1000);      /* thousand */ 
        number -= Hn * 1000; 
        var Dn = Math.floor(number / 100);       /* Tens (deca) */ 
        number = number % 100;               /* Ones */ 
        var tn= Math.floor(number / 10); 
        var one=Math.floor(number % 10); 
        var res = ""; 
        if (Gn>0){ 
            res += (ones[Gn] + " CRORE"); 
        } 
        if (kn>0){ 
            res += (((res=="") ? "" : " ") + 
            ones[kn] + " Lakh"); 
    } 
    if (Hn>0) 
    { 
        res += (((res=="") ? "" : " ") +
            ones[Hn] + " Thousand"); 
    } 

    if (Dn) 
    { 
        res += (((res=="") ? "" : " ") + 
            ones[Dn] + " Hundred"); 
    } 


    if (tn>0 || one>0) 
    { 
        if (!(res=="")) 
        { 
            res += " And "; 
        } 
        if (tn < 2) 
        { 
            res += ones[tn * 10 + one]; 
        } 
        else 
        { 

            res += tens[tn];
            if (one>0) 
            { 
                res += ("-" + ones[one]); 
            } 
        } 
    }

    if (res=="")
    { 
        res = "zero"; 
    } 
    return res;
},

    add_product:function(product, options){
        _super_order.add_product.call(this, product, options);
        var line=this.get_last_orderline();
        if(line && line.product.id===product.id && product.not_refunded===true){
            line.set_not_refund(product);
        }
    },
});
var _super_order_line = pos_model.Orderline.prototype;
pos_model.Orderline = pos_model.Orderline.extend({
    initialize: function(attr,options){
        _super_order_line.initialize.call(this, attr, options);
        this.set({'not_refundable': false});
    },
    init_from_JSON: function(json) {
        return _.extend(_super_order_line.init_from_JSON.apply(this,arguments), {
            not_refundable:this.not_refundable,
        });
    },
    export_as_JSON: function () {
        return _.extend(_super_order_line.export_as_JSON.apply(this, arguments), {
            not_refundable:this.not_refundable,
        });
    },
    set_not_refund:function(product){
        return this.not_refundable=product.not_refunded;
    }
    
 });
 var PosModelSuper=pos_model.PosModel;
pos_model.PosModel = pos_model.PosModel.extend({
    
    _flush_orders: function (orders, options) {
        var result_new = PosModelSuper.prototype._flush_orders.call(this, orders, options);
        var self = this;
        var new_order = {};
        var return_order_list = self.orders;
        self.load_new_orders();
        return result_new;
    },
load_new_orders: function(){
        var self = this;
        var def  = new $.Deferred();
        var fields = _.find(this.models,function(model){ return model.model === 'pos.order'; }).fields;
        var domain =[['state', '!=', 'draft'], ['state', '!=', 'cancel'], ['is_refund', '=', false],['refund_type', '!=','full']]
        rpc.query({
                model: 'pos.order',
                method: 'search_read',
                args: [domain, fields],
            }, {
                timeout: 3000,
                shadow: true,
            }).then(function(orders){
                if (self.db.add_orders(orders)) {   // check if the partners we got were real updates
                    def.resolve();
                } else {
                    def.reject();
                }
            }, function(type,err){ def.reject(); }); 
        return def;
    },


load_new_order_lines: function(domain){
        var self = this;
        var def  = new $.Deferred();
        var fields = _.find(this.models,function(model){ return model.model === 'pos.order.line'; }).fields;
        rpc.query({
                model: 'pos.order.line',
                method: 'search_read',
                args: [domain, fields],
            }, {
                timeout: 3000,
                shadow: true,
            })
            .then(function(lines){
                if (self.db.add_order_lines(lines)) {  // check if the partners we got were real updates
                    def.resolve();
                } else {
                    def.reject();
                }
            }, function(type,err){ def.reject(); });
        return def;
    },
});

var OrderReprintWidget = screens.ScreenWidget.extend({
        template: 'OrderReprintWidget',
        init: function(parent) {
            return this._super(parent);
        },
        
        show: function(){
            var self = this;
            this._super();
            var result=this.pos.reprint_result;
            this.render_posreceipt(result);
            this.$('.back').click(function(){
                self.gui.show_screen('refund_screen');
            });
            this.$('.print').click(function(){
                self.print_order()
                });
            
        },
        print_order:function(){
            var order=this.pos.reprint_order;
            this.gui.screen_instances.receipt.print();
        },
        render_posreceipt: function(result){
            var order=this.pos.reprint_order;
            var contents = this.$el[0].querySelector('.order-details-contents');
            contents.innerHTML = "";
            var el_str =  QWeb.render('PosTicketOld',{widget:this, order:order, change: result[3],
                    orderlines: result[0],
                    discount_total: result[1],
                    paymentlines: result[2],});
            var el_node = document.createElement('div');
                el_node.innerHTML = _.str.trim(el_str);
                el_node = el_node.childNodes[0];
                el_node.order = order;
            contents.appendChild(el_node);
        },
});

gui.define_screen({name:'order_reprint', widget: OrderReprintWidget});

})
