odoo.define('pos_refund_order_extended.popups', function (require) {
"use strict";

var PopupWidget = require('point_of_sale.popups');
var gui = require('point_of_sale.gui');
var core = require('web.core');
var QWeb = core.qweb;
var models = require('point_of_sale.models');
var db = require('point_of_sale.DB');

var RefundOrderDetailWidget = PopupWidget.extend({
    template: 'RefundOrderDetailWidget',
    events:{
        'click .button.cancel':  'click_cancel',
        'click .button.confirm': 'click_confirm',
        'click .selection-item': 'click_item',
        'click .input-button':   'click_numpad',
        'click .mode-button':    'click_numpad',
        'click .button.complete_return':  'click_complete_returns',
    },
    show: function(options){
        options = options || {};
        this._super(options);
        var order = this.pos.order_selected;
        if (order){
            this.lines = this.pos.db.get_line_by_pos_reference(order.pos_reference);
        }
        if (order){
            this.$('span[name=error_msg]').addClass('oe_hidden');
        }
        this.$('.completeret').click(function(){
            });
        this.renderElement();
        
    },
    click_complete_returns:function(){
        var order = this.pos.order_selected;
        var lines = this.pos.db.get_line_by_pos_reference(this.pos.order_selected.pos_reference);
        if (lines){
            for(var i = 0; i < lines.length; i++){
                var line = lines[i];
                var product = this.pos.db.get_product_by_id(line.product_id[0]);
                var name=line.id;
                var old_total_qty=old_total_qty+line.qty;
                var value='input[name=';
                this.$(value.concat(name,']')).val(line.qty-line.refund_qty);
            }
        }
        
    },
    click_confirm: function(){
        var self = this;
        var lines = this.pos.db.get_line_by_pos_reference(this.pos.order_selected.pos_reference);
        var cache_order = this.pos.db.get_order_by_code_reference(this.pos.order_selected.pos_reference);
        var uid = cache_order. pos_reference.split(' ')[1];
        var orders = this.pos.get('orders').models;
        var old_order = orders.find(function (order) {
            return order.old_uid === uid;
        });
        if (old_order) {
            this.pos.set('selectedOrder', old_order);
            this.gui.show_screen('products');
            return;
        }
        else if (lines && !old_order) {
            var options = {
                pos_session_id: this.pos.pos_session.id,
                pos: this.pos,
            }
            var new_order = new models.Order({}, options);
            var old_lines = {};
            var new_total_qty=0.00;
            var old_total_qty=0.00;
            new_order['old_uid'] = uid
            for(var i = 0; i < lines.length; i++){
                var line = lines[i];
                var product = this.pos.db.get_product_by_id(line.product_id[0]);
                var name=line.id;
                var old_total_qty=old_total_qty+(line.qty-line.refund_qty);
                var value='input[name=';
                var qty=this.$(value.concat(name,']')).val();
                var new_total_qty=new_total_qty+Number(qty);
                if (Number(qty)>line.qty){
                    this.$(value.concat(name,']')).addClass('error');
                    this.$(value.concat(name,']')).focus();
                    this.$('span[name=error_msg]').removeClass('oe_hidden');
                    return;
                }
                if (qty>0){
                new_order.add_product(product, {
                    price:  line.price_unit,
                    quantity: - qty,
                    discount: line.discount,
                })
                old_lines[line.id] = Number(qty);
                var new_line = new_order.get_last_orderline();
                
                new_line['is_refund'] = true;
                new_line['refund_date'] = new Date().toLocaleTimeString();
                if (!new_order['source']) {
                    new_order['source'] = line.name
                    new_order['parent_id'] = line.order_id[0]
                }
                };
            }
            if (this.pos.order_selected.partner_id && this.pos.order_selected.partner_id[0]) {
                new_order.set_client(this.pos.db.get_partner_by_id(this.pos.order_selected.partner_id[0]));
            }
            new_order['is_refund'] = true;
            new_order['old_lines'] = old_lines;
            if (new_total_qty===old_total_qty){
                new_order['refund_type']='full';
            }
            else if(new_total_qty<=old_total_qty && new_total_qty!==0.0){
                new_order['refund_type']='partial';
            }
            this.gui.close_popup();
            new_order.trigger('change', new_order);
            this.pos.get('orders').add(new_order);
            this.pos.set('selectedOrder', new_order);
            this.gui.show_screen('products');
            return new_order;
        }
        if( this.options.confirm ){
            this.options.confirm.call(this);
        }
    },
});
gui.define_popup({name:'refund-line-info-input', widget: RefundOrderDetailWidget});

return PopupWidget;
});
