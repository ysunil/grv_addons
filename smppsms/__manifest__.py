# -*- coding: utf-8 -*-
##########################################################################
# Author      : KnackTechs Solutions Software Pvt. Ltd. (<https://knacktechs.com/>)
# Copyright(c): 2017-Present KnackTechs Solutions Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.knacktechs.com/license.html/>
##########################################################################
{
    "name":  "SMPP SMS Gateway",
    "summary":  "This module allows you to send sms notification to your users for the order confirmation, delivery, refund, etc.",
    "category":  "Marketing",
    "version":  "1.2.0",
    "sequence":  1,
    "author":  "Knacktechs Solutions",
    "license":  "Other proprietary",
    "website":  "www.knacktechs.com",
    "description":  """www.knacktechs.com""",
    "depends":  ['sms_notification','sale','point_of_sale','mail'],
    'data': [   
                'views/inherit_menu.xml',
                'views/sms_config_view.xml',
                'views/sms_log_file_view.xml',
                'edi/sms_template_for_partner_create_pos.xml',
                'edi/sms_template_for_order_refund_pos.xml',
                'edi/sms_template_for_order_confirm_pos.xml',
            ],
#    "images":  ['static/description/Banner.png'],
    "application":  True,
    "installable":  True,
    "auto_install":  False,
    "price":  5000,
    "currency":  "INR",
}
