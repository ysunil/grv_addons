# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 KnackTechs
# Author : www.knacktechs.com
#
##############################################################################

from . import sms_config
from . import sms_log_file
from . import pos_order
from . import res_partner
from . import sms_base
from . import sms_mail_server
from . import sms_template
from . import sms_sms

