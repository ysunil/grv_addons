from odoo import models,api,fields
import re

class SmsTemplate(models.Model):
    "Templates for sending sms"
    _inherit = "sms.template"
    _description = 'SMS Templates'
    _order = 'name'
    
    
    @api.multi
    def get_body_data(self, obj):
        self.ensure_one()
        if obj:
            if 'res.partner' in self.model:
                obj.partner_id=obj
            body_msg = self.env["mail.template"].with_context(lang=obj.partner_id.lang).sudo().render_template(
                self.sms_body_html, self.model, [obj.id])
            new_body_msg = re.sub("<.*?>", " ", body_msg[obj.id])
            return new_body_msg
            return " ".join(strip_tags(new_body_msg).split())
    
    @api.model
    def send_sms_using_template(self, mob_no, sms_tmpl, sms_gateway=None, obj=None):
        if not sms_gateway:
            gateway_id = self.env["sms.mail.server"].search(
                [], order='sequence asc', limit=1)
        else:
            gateway_id = sms_gateway
        msg=sms_tmpl.get_body_data(obj)
        if 'res.partner' == sms_tmpl.model:
            obj.partner_id=obj
        if mob_no and sms_tmpl and obj:
            sms_sms_obj = self.env["sms.sms"].create({
                'sms_gateway_config_id': gateway_id.id,
                'partner_id': obj.partner_id.id,
                'to': mob_no,
                'group_type': 'individual',
                'auto_delete': sms_tmpl.auto_delete,
                'msg': msg,
                'template_id': False
            })
            result=sms_sms_obj.send_sms_via_gateway(
                sms_sms_obj.msg, [sms_sms_obj.to], from_mob=None, sms_gateway=gateway_id)
            model_id=self.env['ir.model'].search([('model','=',sms_tmpl.model)])
            self.env['sms.log'].create({ 'error_code':result['ErrorCode'],
                                        'error_msg':result['ErrorMessage'],
                                        'partner_id':obj.partner_id.id,
                                        'model_id':obj.id,
                                        'model':model_id.id,
                                        'model_name':sms_tmpl.model
                                        })
        return True

    
    
    @api.depends('condition')
    def onchange_condition(self):
        if self.condition:
            if self.condition in ['order_placed', 'order_confirm', 'order_cancel']:
                model_id = self.env['ir.model'].search(
                    [('model', '=', 'sale.order')])
                self.model_id = model_id.id if model_id else False
            elif self.condition in ['order_delivered']:
                model_id = self.env['ir.model'].search(
                   [('model', '=', 'stock.picking')])
                self.model_id = model_id.id if model_id else False
            elif self.condition in ['invoice_vaildate', 'invoice_paid']:
                model_id = self.env['ir.model'].search(
                    [('model', '=', 'account.invoice')])
                self.model_id = model_id.id if model_id else False
            elif self.condition in ['order_refund_pos','order_confirm_pos']:
                model_id = self.env['ir.model'].search(
                    [('model', '=', 'pos.order')])
                self.model_id = model_id.id if model_id else False
            elif self.condition in ['pos_partner_create']:
                model_id = self.env['ir.model'].search(
                    [('model', '=', 'res.partner')])
                self.model_id = model_id.id if model_id else False
        else:
            self.model_id = False

    condition = fields.Selection([('order_placed', 'Order Placed'),
                                  ('order_confirm', 'Order Confirmed'),
                                  ('order_confirm_pos', 'Order Confirmed POS'),
                                  ('order_refund_pos', 'Order Refund POS'),
                                  ('pos_partner_create', 'POS Partner Creation'),
                                  ('order_delivered', 'Order Delivered'),
                                  ('invoice_vaildate', 'Invoice Validate'),
                                  ('invoice_paid', 'Invoice Paid'),
                                  ('order_cancel', 'Order Cancelled')], string="Conditions", help="Condition on which the template has been applied.")
