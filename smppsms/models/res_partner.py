from odoo import models,api

class Partner(models.Model):
    _inherit = "res.partner"
    _description = "Sending SMS while creating a partner in POS Screen"
    
    @api.model
    def create_from_ui(self, partner):
        partner_create = super(Partner, self).create_from_ui(partner)
#        vals=============== {'lang': u'en_US', u'city': u'nfhfthtf', u'name': u'abcv', u'zip': u'4565645', u'barcode': u'5675675675', u'country_id': u'233', u'phone': u'464575675', u'street': u'fghfhyt', u'property_product_pricelist': 1, u'email': u'fdgergre', u'vat': False}
        sms_template_objs = self.env["sms.template"].sudo().search(
            [('condition', '=', 'pos_partner_create')])
        for sms_template_obj in sms_template_objs:
            mobile = sms_template_obj._get_partner_mobile(self.browse(partner_create))
            if mobile:
                sms_template_obj.send_sms_using_template(
                    mobile, sms_template_obj, obj=self.browse(partner_create))

                   
