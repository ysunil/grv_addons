from odoo import api, fields, models, _

class SmsLogFile(models.Model):
    _name='sms.log'
    _rec_name='error_code'
    
    error_code=fields.Char('Error Code')
    error_msg=fields.Text('Error Msg')
    model_id=fields.Integer('ID')
    partner_id=fields.Many2one('res.partner', "Partner")
    model=fields.Many2one('ir.model', "Model")
    model_name = fields.Char(related="model.model", string='Related Document Model',
                        store=True, readonly=True)
SmsLogFile()