from odoo import models,api,fields


class SmsMailServer(models.Model):
    _inherit = "sms.mail.server"
    _description = "Addings SMPP as a mail gateway in SMS Notification Module"
    
    gateway = fields.Selection([
        ('smpp', 'SMPPSMS Gateway'),
    ], string="SMS Gateway")