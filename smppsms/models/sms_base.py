from odoo import SUPERUSER_ID, api
from odoo.exceptions import UserError, ValidationError

import re
from functools import reduce
import operator
from odoo.exceptions import except_orm, Warning, RedirectWarning
from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)
from xmlrpc import client
import requests
import xmltodict
import json
import ast


class ApiRequest:
   # standard api request to ingram
    def send_api_request(self,method,http_header, http_request_vals,url):
        result = requests.request(method,url, params=http_request_vals, headers=http_header,verify=False)
   # convert result to python dictionary more like json
        result_dict = json.loads(result.text)
        return result_dict
    
    def reply_msg_decode_smpp(self, result_dict):
        if result_dict.get('ErrorCode')=='000':
            raise UserError('Done!!!')
        elif result_dict.get('ErrorCode')=='001':
            raise UserError('login details cannot be blank!!!')
        elif result_dict.get('ErrorCode')=='003':
            raise UserError('sender cannot be blank!!!')
        elif result_dict.get('ErrorCode')=='004':
            raise UserError('message text cannot be blank!!!')
        elif result_dict.get('ErrorCode')=='005':
            raise UserError('message data cannot be blank!!!')
        elif result_dict.get('ErrorCode')=='006':
            raise UserError('error: generic error description!!!')
        elif result_dict.get('ErrorCode')=='007':
            raise UserError('username or password is invalid!!!')
        elif result_dict.get('ErrorCode')=='008':
            raise UserError('account not active!!!')
        elif result_dict.get('ErrorCode')=='009':
            raise UserError('account locked, contact your account manager!!!')
        elif result_dict.get('ErrorCode')=='010':
            raise UserError('api restriction!!!')
        elif result_dict.get('ErrorCode')=='011':
            raise UserError('ip address restriction!!!')
        elif result_dict.get('ErrorCode')=='012':
            raise UserError('invalid length of message text!!!')
        elif result_dict.get('ErrorCode')=='013':
            raise UserError('mobile numbers not valid!!!')
        elif result_dict.get('ErrorCode')=='014':
            raise UserError('account locked due to spam message contact support!!!')
        elif result_dict.get('ErrorCode')=='015':
            raise UserError('senderid not valid!!!')
        elif result_dict.get('ErrorCode')=='017':
            raise UserError('groupid not valid!!!')
        elif result_dict.get('ErrorCode')=='018':
            raise UserError('multi message to group is not supported!!!')
        elif result_dict.get('ErrorCode')=='019':
            raise UserError('schedule date is not valid!!!')
        elif result_dict.get('ErrorCode')=='020':
            raise UserError('message or mobile number cannot be blank!!!')
        elif result_dict.get('ErrorCode')=='021':
            raise UserError('insufficient credits!!!')
        elif result_dict.get('ErrorCode')=='022':
            raise UserError('invalid jobid!!!')
        elif result_dict.get('ErrorCode')=='023':
            raise UserError('parameter missing!!!')
        elif result_dict.get('ErrorCode')=='024':
            raise UserError('invalid template or template mismatch!!!')
        elif result_dict.get('ErrorCode')=='025':
            raise UserError('{Field} can not be blank or empty!!!')
        elif result_dict.get('ErrorCode')=='026':
            raise UserError('invalid date range!!!')
        elif result_dict.get('ErrorCode')=='027':
            raise UserError('invalid optin user!!!')
        else:
            raise UserError('Error in SMPP!!')
        

    


class SmsBase(models.AbstractModel):
    _inherit = "sms.base.abstract"
    _description = "Contains the logic shared between models which allows to send sms."
    
    ApiRequest = ApiRequest()
    
    @api.multi
    def send_sms_via_gateway(self, body_sms, mob_no, from_mob=None, sms_gateway=None):
        print ("Mobile Number to---------%s"%mob_no)
        result = super(SmsBase, self).send_sms_via_gateway(body_sms, mob_no, from_mob, sms_gateway)
        if result and result.gateway=='smpp':
            mob_no=mob_no[0][3:]
            print ("mobile number now is equalo tooo======%s"%mob_no)
            smpp_config_obj = self.env["sms.config"].sudo().search(
                [],limit=1)
            http_request_vals={}
            http_header={}
            url=smpp_config_obj.url
            method=smpp_config_obj.method
            if smpp_config_obj and smpp_config_obj.url_ids and smpp_config_obj.method:
                for url_key in smpp_config_obj.url_ids:
                    if url_key.type_value=='headers':
                        http_header[url_key.key]=url_key.value
                    elif url_key.type_value=='key_value':
                        http_request_vals[url_key.key]=url_key.value
                    elif url_key.type_value=='mobile':
                        http_request_vals[url_key.key]=mob_no
                    elif url_key.type_value=='message':
                        http_request_vals[url_key.key]=body_sms
            else:
                raise UserError('No Configuration found for SMPPSMS!!!!!')
            xml_response=self.ApiRequest.send_api_request(method,http_header, http_request_vals,url)
            return xml_response
        else:
            return
