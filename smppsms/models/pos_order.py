from odoo import models,api

class PosOrder(models.Model):
    _inherit = "pos.order"
    _description = "Sending SMS while creating or refunding order in POS Screen"
    
    
    @api.model
    def _process_order(self, pos_order):
        pos_order_result = super(PosOrder, self)._process_order(pos_order)
        sms_template_objs = self.env["sms.template"].sudo().search(
            [('condition', '=', 'order_confirm_pos')])
        for sms_template_obj in sms_template_objs:
            mobile = sms_template_obj._get_partner_mobile(pos_order_result.partner_id)
#            print "mobilemobile",mobile
            if mobile:
                sms_template_obj.send_sms_using_template(
                    mobile, sms_template_obj, obj=pos_order_result)
        return pos_order_result
