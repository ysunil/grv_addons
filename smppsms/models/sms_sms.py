from odoo import models,api

class SmsSms(models.Model):
    _inherit = "sms.sms"
    
    def _get_partner_mobile(self, partner):
        company_country_calling_code = self.env.user.company_id.country_id.phone_code
        managed_calling_code = self.env['ir.config_parameter'].get_param(
            'sms_notification.is_phone_code_enable', 'False') == 'True'
        if managed_calling_code:
            return partner.mobile
        if partner.country_id and partner.country_id.phone_code:
            country_calling_code = partner.country_id.phone_code
        else:
            country_calling_code = company_country_calling_code
        return "+{code}{mobile}".format(code=country_calling_code, mobile=partner.mobile)


                   
