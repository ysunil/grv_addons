from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import requests
import xmltodict
import json

class ApiRequest:
   # standard api request to ingram
    def send_api_request(self, xml_request, url):
        headers = {'Content-Type': 'application/xml'}
        result = requests.request("GET", url, headers=headers, params=xml_request)
        print("Result Returned--------from SMPP------ %s" % result.text)
   # convert result to python dictionary more like json
        try:
            result_dict = json.dumps(dict(xmltodict.parse(result.text, xml_attribs=True)), indent=4)
            result_dict = json.loads(result_dict)
        except Exception:
            raise UserError('Something went wrong from Synnex end')
        return result_dict
    
class SMSConfig(models.Model):
    _name='sms.config'
    
    ApiRequest = ApiRequest()
    
    name=fields.Char('Name')
    url=fields.Char('URL')
    method=fields.Char('Method')
    url_ids=fields.One2many('sms.url.config','sms_config_id', string='URL Key')
    
    @api.multi
    def sms_int(self):
        xml_response=self.ApiRequest.send_api_request(xml_request, self.url)
        print("xml_responsexml_response==== %s" % xml_response)


class SmsUrlConfig(models.Model):
    _name='sms.url.config'
    
    sms_config_id=fields.Many2one('sms.config')
    type_value = fields.Selection([('headers', 'Headers'),
                                  ('key_value', 'Key And Value'),
                                  ('mobile', 'Mobile Key'),
                                  ('message', 'Message Key')],string="Type")
    key=fields.Char('Key')
    value=fields.Char('Value')