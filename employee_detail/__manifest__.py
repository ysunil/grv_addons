# -*- coding: utf-8 -*-
##########################################################################
# Author      : Knacktechs Solutions (<https://knacktechs.com/>)
# Copyright(c): 2017-Present Knacktechs Solutions 
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.knacktechs.com/license.html/>
##########################################################################
{
    "name":  "Employee Detials",
    "summary":  "Knacktechs Solutions",
    "category":  "Employee",
    "version":  "1.2.0",
    "sequence":  1,
    "author":  "Knacktechs Solutions",
    "license":  "Other proprietary",
    "website":  "www.knacktechs.com",
    "description":  """""",
    "depends":  ['base','hr' ],
    'data': [
        'views/employee_view.xml',
        'views/employee_appraisal_view.xml',
    ],
    "application":  True,
    "installable":  True,
    "auto_install":  False,
}
