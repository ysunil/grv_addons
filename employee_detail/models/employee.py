from odoo import fields, models
#from odoo.exceptions import ValidationError

class Employee(models.Model):
    _inherit = 'hr.employee'    
    
    passport_expiry = fields.Date(string='Expiry')
    identity_expiry = fields.Date(string='Expiry')
    emirate_id = fields.Char(string='Emirates Id.')
    emirate_expiry = fields.Date(string='Expiry')
    other_doc = fields.Char(string='Other Doc.')
    laybour_card = fields.Char(string='Laybour Card No.')
    laybour_expiry = fields.Date(string='Expiry')
    other_expiry = fields.Date(string='Expiry')
    
    _sql_constraints = [
        ('emirate_id_company_uniq', 'unique (emirate_id,company_id)', 'The Emirates Id of the Employee must be unique per company !'),
        ('passport_id_company_uniq', 'unique (passport_id,company_id)', 'The Passport No of the Employee must be unique per company !')
    ]
    
#    @api.onchange('emirate_id')
#    def onchange_emirates_id(self):
#        """checking emirates id format"""
#        if self.emirate_id:
#            if not re.match('^[0-9]{3}-[0-9]{4}-[0-9]{7}-[0-9]{1}$', self.emirate_id):
#                self.emirate_id = None
#                raise ValidationError(_('''Incorrect format for emirates id, it should be like 'xxx-xxxx-xxxxxxx-x'''))



    
    




    
    
    
    

