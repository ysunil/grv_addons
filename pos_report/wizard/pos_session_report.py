from odoo import api, fields, models, _
import datetime

class PosSessionReport(models.TransientModel):
    _name='pos.session.report'
    
    session_ids=fields.Many2many('pos.session','session_pos_report', 'report_id', 'session_id', string='Session(s)')
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
        default=lambda self: self.env['res.company']._company_default_get('account.dayend.report'))
    start_date = fields.Date(string="Start Date")
    end_date = fields.Date(string="End Date")
    
    def payment_summery_with_tax(self):
        final_dict=[]
        if self:
            for record in self:
                for session in record.session_ids:
                    if session.order_ids:
                        for order in session.order_ids:
                            if order.statement_ids and order.lines:
                                statement_count= len(order.statement_ids)
                                for line in order.lines:
                                    tax_id = line.tax_ids
                                    update=False
                                    count=1
                                    for statement in line.order_id.statement_ids:
                                        tax_amount=(line.price_subtotal_incl-line.price_subtotal)/len(order.statement_ids) or 0.0
                                        total=statement.amount
                                        price_subtotal=statement.amount-tax_amount
                                        if final_dict:
                                            for i, lines in enumerate(final_dict):
                                                if lines['payment_by'] == statement.journal_id and lines['tax']==tax_id:
                                                    if count==statement_count:
                                                        update=True
                                                    lines['subtotal']=lines['subtotal']+price_subtotal
                                                    lines['tax_amount']=lines['tax_amount']+tax_amount
                                                    lines['total']=lines['total']+total
                                                    count+=1
                                            if update==False:
                                                final_dict.append({'payment_by': statement.journal_id,
                                                                    'tax': tax_id,
                                                                    'tax_amount': tax_amount,
                                                                    'subtotal': price_subtotal,
                                                                    'total': total})
                                        else:
                                            vals = {'payment_by': statement.journal_id,
                                                                    'tax': tax_id,
                                                                    'tax_amount': tax_amount,
                                                                    'subtotal': price_subtotal,
                                                                    'total': total}
                                            final_dict.append(vals)
                return final_dict
    @api.multi
    def print_report(self):
        self.ensure_one()
        self.sent = True
        return self.env.ref('pos_report.pos_session_report').report_action(self)

    @api.onchange('start_date','end_date')
    def onchange_start_date(self):
        if self.start_date and self.end_date:
            start_date_time=datetime.datetime.strptime(str(self.start_date), "%Y-%m-%d").strftime('%Y-%m-%d 00:00:00')
            end_date_time=datetime.datetime.strptime(str(self.end_date), "%Y-%m-%d").strftime('%Y-%m-%d 00:00:00')
            session_ids = self.env['pos.session'].search([('start_at','>',start_date_time),('start_at','<',end_date_time)])
            self.session_ids=[(6, 0, [session.id for session in session_ids])]
            


PosSessionReport()









 