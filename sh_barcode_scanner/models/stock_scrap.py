# -*- coding: utf-8 -*-
# Copyright (C) Softhealer Technologies.

from odoo import models,fields,api

class StockScrap(models.Model):
    _inherit='stock.scrap'
 
    barcode = fields.Char("Barcode")
    
    @api.onchange('barcode')
    def barcode_scanned(self):

        if self and self.barcode:
            
            product_obj = self.env['product.product'].search([('barcode', '=', self.barcode)],limit=1)
            if product_obj:
                
                if self.product_id:
                    if self.product_id.barcode == self.barcode:
                        self.scrap_qty += 1
                    else:
                        self.barcode = ''
                        self._cr.commit()
                        warning_mess = {
                            'message' : ('Scrap Product changed, please save current product scrap order and continue with next order.'),
                            'title': "Warning" 
                        }
                        return {'warning': warning_mess}         
                        
                else:
                    self.product_id = product_obj.id
            
                self.barcode = ''
            else :
                self.barcode = ''
                self._cr.commit()
                warning_mess = {
                    'message' : ('Product with this barcode does not exist.'),
                    'title': "Warning" 
                }
                return {'warning': warning_mess}         
