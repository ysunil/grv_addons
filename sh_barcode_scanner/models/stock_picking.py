# -*- coding: utf-8 -*-
# Copyright (C) Softhealer Technologies.

from odoo import models,api
from odoo.exceptions import Warning,UserError

class StockPicking(models.Model):
    _inherit='stock.picking'
 
    @api.one
    def sh_on_barcode_scanned(self, barcode):
                
        picking_id =self.id
        stock_picking = self.env['stock.picking'].search([('id', '=', picking_id)],limit=1)       
            
        if stock_picking:
            product_id = self.env['product.product'].search([('barcode', '=', barcode)],limit=1)
    
            if product_id:
                stock_picking_line = stock_picking.move_lines.search([('product_id', '=', product_id.id),('picking_id','=',picking_id)], limit=1)
                     
                if stock_picking_line:         
                    stock_picking_line.quantity_done += 1
                    self._cr.commit()
                    
                    if stock_picking_line.quantity_done  == stock_picking_line.product_qty :
                        raise Warning('Done quantity exceed required quantity')       
    
            else :
                raise UserError('Product does not exist')
             