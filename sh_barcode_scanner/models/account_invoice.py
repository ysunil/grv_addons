# -*- coding: utf-8 -*-
# Copyright (C) Softhealer Technologies.

from odoo import models, api
from odoo.exceptions import UserError

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    @api.one
    def sh_on_barcode_scanned(self, barcode):
        
        invoice_id = self.id    

        if self.type=="out_invoice":  # Sales Invoice
            sale_invoice = self.env['account.invoice'].search([('id', '=', invoice_id),('type','=','out_invoice')],limit=1)
                
            if sale_invoice:                
                product_id = self.env['product.product'].search([('barcode', '=', barcode)],limit=1)        
                     
                if product_id:     
                    if not sale_invoice.invoice_line_ids:    
                        invoice_line_val = {
                            'name': product_id.name,
                            'product_id': product_id.id,
                            'account_id':product_id.property_account_income_id,   
                            'quantity': 1,
                            'product_uom': product_id.product_tmpl_id.uom_id.id,
                            'price_unit': product_id.product_tmpl_id.list_price,
                            'invoice_id': sale_invoice.id
                        }
                        sale_invoice.update({'invoice_line_ids': [(0, 0, invoice_line_val)]})
                 
                    else:
                        sale_invoice_line = sale_invoice.invoice_line_ids.search([('product_id', '=', product_id.id),('invoice_id','=',invoice_id)], limit=1)                
                             
                        if sale_invoice_line:
                            sale_invoice_line.quantity = sale_invoice_line.quantity + 1
                        
                        else:
                            invoice_line_val = {
                                'name': product_id.name,
                                'product_id': product_id.id,
                                'account_id':product_id.property_account_income_id,   
                                'quantity': 1,
                                'product_uom': product_id.product_tmpl_id.uom_id.id,
                                'price_unit': product_id.product_tmpl_id.list_price,
                                'invoice_id': sale_invoice.id
                            }
                            sale_invoice.update({'invoice_line_ids': [(0, 0, invoice_line_val)]})
                else :
                    raise UserError('Product does not exist')


        elif self.type=="in_invoice":  # Purchase  Invoice            
            purchase_invoice = self.env['account.invoice'].search([('id', '=', invoice_id),('type','=','in_invoice')],limit=1)       
                
            if purchase_invoice:      
                product_id = self.env['product.product'].search([('barcode', '=', barcode)],limit=1)        
                      
                if product_id:      
                    if not purchase_invoice.invoice_line_ids:    
                        invoice_line_val = {
                            'name': product_id.name,
                            'product_id': product_id.id,
                            'account_id':product_id.property_account_expense_id,   
                            'quantity': 1,
                            'product_uom': product_id.product_tmpl_id.uom_id.id,
                            'price_unit': product_id.product_tmpl_id.list_price,
                            'invoice_id': purchase_invoice.id
                        }
                        purchase_invoice.update({'invoice_line_ids': [(0, 0, invoice_line_val)]})
                  
                    else:
                        sale_invoice_line = purchase_invoice.invoice_line_ids.search([('product_id', '=', product_id.id),('invoice_id','=',invoice_id)], limit=1)                
                              
                        if sale_invoice_line:
                            sale_invoice_line.quantity = sale_invoice_line.quantity + 1
                        else:
                            invoice_line_val = {
                                'name': product_id.name,
                                'product_id': product_id.id,
                                'account_id':product_id.property_account_expense_id,   
                                'quantity': 1,
                                'product_uom': product_id.product_tmpl_id.uom_id.id,
                                'price_unit': product_id.product_tmpl_id.list_price,
                                'invoice_id': purchase_invoice.id
                            }
                            purchase_invoice.update({'invoice_line_ids': [(0, 0, invoice_line_val)]})
                else :
                    raise UserError('Product does not exist')
                 
            
                        
