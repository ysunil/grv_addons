odoo.define('pos_check.popups', function (require) {
"use strict";

var PopupWidget = require('point_of_sale.popups');
var gui = require('point_of_sale.gui');

var CheckInfoWidget = PopupWidget.extend({
    template: 'CheckInfoWidget',
    show: function(options){
        options = options || {};
        this._super(options);
        this.renderElement();
        $('body').off('keypress', this.keyboard_handler);
        $('body').off('keydown', this.keyboard_keydown_handler);
        window.document.body.addEventListener('keypress',this.keyboard_handler);
        window.document.body.addEventListener('keydown',this.keyboard_keydown_handler);
        if(options.data){
            var data = options.data;
            this.$('input[name=check_bank_name]').val(data.check_bank_name);
            this.$('input[name=check_number]').val(data.check_number);
            this.$('input[name=check_date]').val(data.check_date);
        }
    },
    click_confirm: function(){
        var infos = {
            'check_bank_name' : this.$('input[name=check_bank_name]').val(),
            'check_number'  : this.$('input[name=check_number]').val(),
            'check_date'  : this.$('input[name=check_date]').val(),
        };
        var valid = true;
        if(this.options.validate_info){
            valid = this.options.validate_info.call(this, infos);
        }

        if(!valid) return;

        this.gui.close_popup();
        if( this.options.confirm ){
            this.options.confirm.call(this, infos);
        }
    },
});
gui.define_popup({name:'check-info-input', widget: CheckInfoWidget});

return PopupWidget;
});