odoo.define('pos_check.models', function (require) {
"use strict";

var models = require('point_of_sale.models');

//load new field 'check_info_required'
models.load_fields("account.journal", "check_info_required");

var paymentline_super = models.Paymentline.prototype;
models.Paymentline = models.Paymentline.extend({
    init_from_JSON: function (json) {
        paymentline_super.init_from_JSON.apply(this, arguments);

        this.check_bank_name = json.check_bank_name;
        this.check_number = json.check_number;
        this.check_date = json.check_date;
    },
    export_as_JSON: function () {
        return _.extend(paymentline_super.export_as_JSON.apply(this, arguments), {
            check_bank_name: this.check_bank_name,
            check_number: this.check_number,
            check_date: this.check_date,
        });
    },
});

var order_super = models.Order.prototype;
models.Order = models.Order.extend({
    add_paymentline_with_check: function(cashregister, infos) {
        this.assert_editable();
        var newPaymentline = new models.Paymentline({},{order: this, cashregister:cashregister, pos: this.pos});
        $.extend(newPaymentline, infos);
        if(cashregister.journal.type !== 'cash' || this.pos.config.iface_precompute_cash){
            newPaymentline.set_amount( Math.max(this.get_due(),0) );
        }
        this.paymentlines.add(newPaymentline);
        this.select_paymentline(newPaymentline);
    },

    update_paymentline_with_check: function(paymentline, infos) {
        this.assert_editable();
        $.extend(paymentline, infos);
        this.select_paymentline(paymentline);
    },
});

return models;
});