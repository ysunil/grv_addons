# -*- coding: utf-8 -*-
from odoo import models

class PosOrder(models.Model):
    _inherit = "pos.order"

    def _payment_fields(self, ui_paymentline):
        res = super(PosOrder, self)._payment_fields(ui_paymentline)
        res.update({
            'check_bank_name': ui_paymentline.get('check_bank_name'),
            'check_number': ui_paymentline.get('check_number'),
            'check_date': ui_paymentline.get('check_date'),
        })
        return res

    def add_payment(self, data):
        statement_id = super(PosOrder, self).add_payment(data)
        StatementLine = self.env['account.bank.statement.line']
        statement_lines = StatementLine.search([
            ('statement_id', '=', statement_id),
            ('pos_statement_id', '=', self.id),
            ('journal_id', '=', data['journal']),
            ('amount', '=', data['amount'])
        ])
        for line in statement_lines:
            if line.journal_id.check_info_required :

                check_vals = {
                    'check_bank_name': data.get('check_bank_name'),
                    'check_number': data.get('check_number'),
                    'check_date': data.get('check_date'),
                }
                line.write(check_vals)
                break

        return statement_id
