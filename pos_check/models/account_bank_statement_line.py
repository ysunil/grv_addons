# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    check_date = fields.Date()
    check_number = fields.Char()
    check_info_required = fields.Boolean(related='journal_id.check_info_required', readonly=True)
    check_bank_name = fields.Char('Check Bank Name')