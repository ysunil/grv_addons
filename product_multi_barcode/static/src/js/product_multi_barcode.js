odoo.define("product_multi_barcode.product_multi_barcode", function (require) {
    "use strict";
    
    var pos_model = require('point_of_sale.models');
    var models = pos_model.PosModel.prototype.models;
    var PosDB = require('point_of_sale.DB');
    models.push(
                {
                    model: 'product.multi.barcode',
                    fields: ['id', 'product_id','barcode',],
                    loaded: function (self, barcode) {
                            self.db.add_barcode(barcode);
                        }
                }
            )
    
    PosDB.include({
        add_barcode: function(barcodes){
            var self=this;
            _.each(barcodes, function (barcode) {
                var product=self.get_product_by_id(barcode.product_id[0]);
                self.product_by_barcode[barcode.barcode] = product;
            });
        }
    })
});