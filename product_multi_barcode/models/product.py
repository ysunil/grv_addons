# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ProductTemplate(models.Model):
    _inherit = "product.template"
    
    multi_barcode=fields.One2many(related='product_variant_ids.multi_barcode', string="Barcods")

class ProductProduct(models.Model):
    _inherit = "product.product"
    
    multi_barcode=fields.One2many('product.multi.barcode','product_id', string="Barcods")

class ProductMultiBarcode(models.Model):
    _name = "product.multi.barcode"
    
    product_id=fields.Many2one('product.product',string="Product")
    barcode=fields.Char('Barcode')