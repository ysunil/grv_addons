# -*- coding: utf-8 -*-
{
    'name': 'Product POS Multi Barcode',
    'version': '10.0.1.0.0',
    'summary': """Product POS Multi Barcode""",
    'category': 'POS',
    'license': 'LGPL-3',
    'author': "Knacktechs Solution",
    'website': "http://knacktechs.com/",
    'depends': ['base','product','point_of_sale'],
    'data': [
        'views/template.xml',
        'views/product_view.xml',
    ],
    'images': [
        'static/description/app.png'
    ],
    'installable': True,
    'application': True,
}
