odoo.define('pos_ticket.models', function (require) {
"use strict";

var models = require('point_of_sale.models');

models.load_fields("res.company",["street","street2","city","zip","vat"]);
models.load_fields("res.partner",["street","street2","city","zip","vat"]);
return models;
});
