# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

{
    "name": "Indian GST",
    "version": "1.0",
    "license": "AGPL-3",
    "description": "Indian GST",
    "depends":  [
                    "base",
                    "account",
                    "stock",
                    "sale",
                    "purchase",
                ],
    "author": "Knacktechs Solutions",
    "category": "Accounting",
    "website": "http://www.knacktechs.com/",
    "data": [
                'data/res.country.state.csv',
                'security/ir.model.access.csv',
                'views/res_country_view.xml',
                'views/chart_account_data.xml',
                'views/account_invoice_view.xml',
                'views/res_partner_view.xml',
                'views/res_company_view.xml',
                'views/account_tax_template_data.xml',
                'views/account_view.xml',
                # 'views/product_view.xml',
                'views/sale_view.xml',
                'views/purchase_view.xml',
                'views/period_view.xml',
                'views/gstr1_view.xml',
                'views/product_hsn_code_view.xml',
                'views/output.xml',
                'views/gst2_view.xml',

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
