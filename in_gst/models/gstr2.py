# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import models, fields, api, _
from odoo.osv import expression, osv
from datetime import datetime, date
import re
import csv
import io
from base64 import b64decode
import logging
import datetime

LOG = logging.getLogger("dicttoxml")
import binascii
import os
import string
import time
import xlwt
import csv
import base64
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition


class Binary(http.Controller):
    @http.route('/opt/download', type='http', auth="public")
    @serialize_exception
    def download_document(self, model, field, id, filename=None, **kw):
        """ Download link for files stored as binary fields.
        :param str model: name of the model to fetch the binary from
        :param str field: binary field
        :param str id: id of the record from which to fetch the binary
        :param str filename: field holding the file's name, if any
        :returns: :class:`werkzeug.wrappers.Response`
        """
        print("selfffffffffffffffffffffffffffffffffffff", self)
        cr, uid, context = request.cr, request.uid, request.context
        env = api.Environment(cr, 1, {})
        out_brw = env['output'].browse(int(id))
        filecontent = base64.b64decode(out_brw.xls_output or '')
        if not filecontent:
            return request.not_found()
        else:
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id)
            return request.make_response(filecontent,
                                         [('Content-Type', 'application/octet-stream'),
                                          ('Content-Disposition', content_disposition(filename))])


class GSTR2(models.Model):
    _name = "gstr.two"
    _rec_name = 'name'

    name = fields.Char(string="Name")
    periods_id = fields.Many2one('fiscal.year.period', string="Period")
    start_period = fields.Date(string="Start Of Period")
    end_period = fields.Date(string="End Of Period")
    state = fields.Selection([
        ('fetch_invoice', 'Fetch Invoice'),
        ('not_upload', 'Not Uploaded'),
        ('ready_to_upload', 'Ready To Upload'),
        ('upload_to_gov', 'Upload To Gov.'),
        ('failed', 'Failed'),
    ], string='Status', index=True, readonly=True, default='fetch_invoice', copy=False, )
    gstr2_bill_details_id = fields.One2many('gstr.two.bill.details', 'gstr2_id')
    company_id = fields.Many2one('res.company', 'Company', required=True)

    @api.onchange('periods_id')
    def _onchange_periods_id(self):
        if self.periods_id.start_period:
            self.start_period = self.periods_id.start_period
        else:
            self.start_period = False
        if self.periods_id.end_period:
            self.end_period = self.periods_id.end_period
        else:
            self.end_period = False

    @api.multi
    def fetch_invoice(self):
        inv_obj = self.env['account.invoice']
        gstr2_bill_details_obj = self.env['gstr.two.bill.details']
        start_period = self.start_period or False
        print("start_periodstart_periodstart_period", start_period)
        end_period = self.end_period or False
        inv_list = []
        inv_ids = inv_obj.search([('date_invoice', '>=', self.start_period), ('date_invoice', '<=', end_period),
                                  ('state', 'in', ('open', 'paid')), ('type', 'in', ('in_invoice', 'in_refund')),
                                  ('company_id', '=', self.company_id.id)])
        # print('-------------',inv_ids, inv_ids.partner_id.inv_type,inv_ids.amount_total)

        if inv_ids:
            for inv_id in inv_ids:
                if inv_id.partner_id.inv_type == 'b2b':
                    inv_type = inv_id.partner_id.inv_type
                elif inv_id.partner_id.inv_type == 'b2cl' and inv_id.amount_total > 250000:
                    inv_type = inv_id.partner_id.inv_type
                elif inv_id.partner_id.inv_type == 'b2cs' and inv_id.amount_total < 250000:
                    inv_type = inv_id.partner_id.inv_type
                else:
                    inv_type = inv_id.partner_id.inv_type
                vals = {
                    'number': inv_id.number or False,
                    'invoice_id': inv_id.id,
                    'partner_id': inv_id.partner_id.id or False,
                    'gstr2_id': self.id or False,
                    'amount_total': inv_id.amount_total or False,
                    'invoice_date': inv_id.date_invoice or False,
                    'invoice_status': inv_id.state or False,
                    'type': inv_id.type or False,
                    'gst_status': 'not_upload',
                    'inv_type': inv_type or False,
                }
                print("valsvalsvalsvalsvalsvalsvals", vals)
                gstr2_bill_details_obj.create(vals)
            self.state = "not_upload"

    @api.multi
    def generate_b2b_csv_file(self):
        #        print'=================='
        cr = self.env.cr
        tax_obj = self.env['account.tax']
        for rec in self:
            if rec.gstr2_bill_details_id:
                for gstr_line in rec.gstr2_bill_details_id:
                    if gstr_line.inv_type == "b2b":
                        output = io.StringIO()
                        writer = csv.writer(output, quoting=csv.QUOTE_ALL)
                        Hedaer_Text = 'Outword Return'
                        writer.writerow(['GSTTIN/UIN of Recipient', 'Invoice Number', 'Invoice Date', 'Invoice Value',
                                         'Place Of Supply', 'Reverse Charge', 'Invoice Type', 'E-commerce GSTIN.',
                                         'Rate', 'Taxable value', 'Cess Amount'])
                        if gstr_line.invoice_id.tax_line_ids:
                            for inv_tax in gstr_line.invoice_id.tax_line_ids:
                                # tax_id = tax_obj.search([('name', '=', inv_tax.name)])
                                writer.writerow([str(gstr_line.invoice_id.partner_id.vat), str(gstr_line.number),
                                                 str(gstr_line.invoice_date), str(gstr_line.amount_total),
                                                 str(gstr_line.invoice_id.partner_id.state_id.name), 'No', 'Regular',
                                                 '',  str(inv_tax.tax_id.amount), ''])
                        cr.execute(""" DELETE FROM output""")
                        values = output.getvalue()
                        attach_id = self.env['output'].create(
                            {'name': Hedaer_Text + '.csv', 'xls_output': base64.b64encode(values.encode('utf-8'))})
                        print ("attach_idattach_idattach_idattach_id", attach_id)
                        return {
                            'type': 'ir.actions.act_url',
                            'url': '/opt/download?model=output&field=xls_output&id=%s&filename=B2B Purchase Return.csv' % (
                                attach_id.id),
                            'target': 'self',
                        }

    @api.multi
    def generate_b2cs_csv_file(self):
        print("selffffffffffffffffff", self)
        cr = self.env.cr
        tax_obj = self.env['account.tax']
        for rec in self:
            if rec.gstr2_bill_details_id:
                for gstr_line in rec.gstr2_bill_details_id:
                    if gstr_line.inv_type == "b2cs":
                        output = io.StringIO()
                        writer = csv.writer(output, quoting=csv.QUOTE_ALL)
                        Hedaer_Text = 'Outword Return'
                        writer.writerow(['Vendor GST','Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Rate',
                                         'Taxable value', 'Cess Amount'])
                        if gstr_line.invoice_id.tax_line_ids:
                            for inv_tax in gstr_line.invoice_id.tax_line_ids:
                                # tax_id = tax_obj.search([('name', '=', inv_tax.name)])
                                writer.writerow([str(gstr_line.invoice_id.partner_id.vat), str(gstr_line.number),
                                                 str(gstr_line.invoice_date), str(gstr_line.amount_total),
                                                 str(gstr_line.invoice_id.partner_id.state_id.name), 'No',
                                                 str(inv_tax.tax_id.amount), ''])
                        cr.execute(""" DELETE FROM output""")
                        values = output.getvalue()
                        attach_id = self.env['output'].create(
                            {'name': Hedaer_Text + '.csv', 'xls_output': base64.b64encode(values.encode('utf-8'))})
                        #                        print ("attach_idattach_idattach_idattach_id",attach_id)
                        return {
                            'type': 'ir.actions.act_url',
                            'url': '/opt/download?model=output&field=xls_output&id=%s&filename=B2CS Purchase Return.csv' % (
                                attach_id.id),
                            'target': 'self',
                        }

    @api.multi
    def generate_b2cl_csv_file(self):
        print("selffffffffffffffffff", self)
        cr = self.env.cr
        tax_obj = self.env['account.tax']
        for rec in self:
            if rec.gstr2_bill_details_id:
                for gstr_line in rec.gstr2_bill_details_id:
                    if gstr_line.inv_type == "b2cl":
                        output = io.StringIO()
                        writer = csv.writer(output, quoting=csv.QUOTE_ALL)
                        Hedaer_Text = 'Outword Return'
                        print("Hedaer_TextHedaer_TextHedaer_TextHedaer_TextHedaer_Text", Hedaer_Text)
                        writer.writerow(
                            ['Vendor GST', 'Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply',
                             'Rate', 'Taxable value', 'Cess Amount'])
                        if gstr_line.invoice_id.tax_line_ids:
                            print("gstr_line.invoice_id.tax_line_ids", gstr_line.invoice_id.tax_line_ids,
                                  gstr_line.invoice_id.tax_line_ids.tax_id)
                            for inv_tax in gstr_line.invoice_id.tax_line_ids:
                                # tax_id = tax_obj.search([('name', '=', inv_tax.name)])
                                # print("tax_idtax_idtax_id", tax_id)
                                writer.writerow([str(gstr_line.invoice_id.partner_id.vat), str(gstr_line.number),
                                                 str(gstr_line.invoice_date), str(gstr_line.amount_total),
                                                 str(gstr_line.invoice_id.partner_id.state_id.name), 'No',
                                                 str(inv_tax.tax_id.amount), ''])
                        cr.execute(""" DELETE FROM output""")
                        print("output.getvalue()output.getvalue()", type(output.getvalue()))
                        values = output.getvalue()
                        print("valuesssssssssssssssssssssssssssssssssss", values)

                        attach_id = self.env['output'].create(
                            {'name': Hedaer_Text + '.csv', 'xls_output': base64.b64encode(values.encode('utf-8'))})
                        print ("attach_idattach_idattach_idattach_id", attach_id)

                        return {
                            'type': 'ir.actions.act_url',
                            'url': '/opt/download?model=output&field=xls_output&id=%s&filename=B2CL Purchase Return.csv' % (
                                attach_id.id),
                            'target': 'self',
                        }

    @api.multi
    def action_return(self):
        self.state = 'fetch_invoice'

    @api.multi
    def generate_csv_file(self):
        self.state = 'ready_to_upload'


class GSTR2BillDetails(models.Model):
    _name = "gstr.two.bill.details"
    _rec_name = 'number'

    number = fields.Char(string="Number")
    invoice_id = fields.Many2one('account.invoice', string="Invoice")
    invoice_date = fields.Date("Bill Date")
    invoice_status = fields.Selection([
        ('paid', 'Paid'),
        ('open', 'Not Paid'),
    ], string='Bill Status', index=True, copy=False, )
    type = fields.Selection([
        ('in_invoice', 'Vendor Bill'),
        ('in_refund', 'Vendor Credit Note'),
    ], string='Type', index=True, copy=False, )
    inv_type = fields.Selection([
        ('b2b', 'B2B'),
        ('b2cl', 'B2CL'),
        ('b2cs', 'B2CS'),
    ], string='Type', index=True, copy=False, )
    gst_status = fields.Selection([
        ('uploaded', 'Uploaded'),
        ('not_upload', 'Not Uploaded'),
    ], string='GST Status', index=True, copy=False, )
    amount_total = fields.Float(string="Total")
    partner_id = fields.Many2one('res.partner', string="Partner")
    gstr2_id = fields.Many2one('gstr.two', string="Partner")

    # str(tax_id.amount),