# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from openerp import models, fields, api,_

class ProductTemplate(models.Model):
    _inherit="product.template"
    
    product_hsn_code_id = fields.Many2one('product.hsn.code', string='HSN Code', change_default=True)
    
    #based on hsn code tax will change
    @api.onchange('product_hsn_code_id')
    def onchange_product_hsn_code_id(self):
        if not self.product_hsn_code_id:
            self.update({
                'taxes_id': False,
                'supplier_taxes_id': False,
            })
        values = {
            'taxes_id': [(6, 0, self.product_hsn_code_id.customer_tax_id.ids)],
            'supplier_taxes_id': [(6, 0, self.product_hsn_code_id.supplier_tax_id.ids)],
        }
        self.update(values)
