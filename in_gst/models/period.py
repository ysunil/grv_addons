# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import models, fields, api,_
from datetime import datetime
from dateutil.relativedelta import relativedelta

class FiscalYears(models.Model):
    
    _name = "fiscal.year"
    _rec_name ='year'
    
    year=fields.Selection([(num, str(num)) for num in range(2017, (datetime.now().year)+20 )], 'Year')
    code=fields.Char(string="Code")
    start_date=fields.Date(string="Start Date")
    end_date=fields.Date(string="End Date")
    fiscal_year_period_id=fields.One2many('fiscal.year.period','fiscal_year_id')
    company_id= fields.Many2one('res.company', 'Company', required=True)
    state = fields.Selection([
            ('draft','Draft'),
            ('running', 'Running'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),
        ], string='Status', index=True, readonly=True, default='draft', copy=False,)
        
    _sql_constraints = [('year_company_uniq', 'unique (year,company_id)', 'The name of the year must be unique per company!')]   
    
    @api.multi
    def create_3_month_period(self):
        return self.create_month_period(3)
    
    @api.multi
    def create_month_period(self,interval=1):
        period_obj = self.env['fiscal.year.period']
        for fy in self:
            ds = datetime.strptime(fy.start_date, '%Y-%m-%d')
            period_obj.create( {
                    'name':  "%s %s" % (_('Opening Period'), ds.strftime('%Y')),
                    'code': ds.strftime('00/%Y'),
                    'start_period': ds,
                    'end_period': ds,
                    'opening_closing_period': True,
                    'fiscal_year_id': fy.id,
                })
            while ds.strftime('%Y-%m-%d') < fy.end_date:
                de = ds + relativedelta(months=1, days=-1)

                if de.strftime('%Y-%m-%d') > fy.end_date:
                    de = datetime.strptime(fy.end_date, '%Y-%m-%d')

                period_obj.create({
                    'name': ds.strftime('%m/%Y'),
                    'code': ds.strftime('%m/%Y'),
                    'start_period': ds.strftime('%Y-%m-%d'),
                    'end_period': de.strftime('%Y-%m-%d'),
                    'fiscal_year_id': fy.id,
                })
                ds = ds + relativedelta(months=1)
            fy.state='running'
        return True
    
class FiscalYearsPeriod(models.Model):
    
    _name = "fiscal.year.period"
    _rec_name ='name'
    
    name=fields.Char(string="Periods Name")
    fiscal_year_id=fields.Many2one('fiscal.year')
    code=fields.Char(string="code")
    start_period=fields.Date(string="Start Of Period")
    end_period=fields.Date(string="End Of Period")
    opening_closing_period=fields.Boolean(string="Opening Closing Period")
    company_id= fields.Many2one(related='fiscal_year_id.company_id',store=True)
    
    _sql_constraints = [('name_company_uniq', 'unique (name,company_id)', 'The name of the period must be unique per company!')]