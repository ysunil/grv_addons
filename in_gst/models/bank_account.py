    
    # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

        
        
class Bank(models.Model):
    _name = 'res.bank'
    _inherit='res.bank'

    bank_line_ids = fields.One2many('bank.branch.lines', 'bank_id', string='Bank Branch Lines', 
        states={'draft': [('readonly', False)]}, copy=True)
class PartnerBank(models.Model):
    _inherit='res.partner.bank'
    
    bank_acc_upi = fields.Char(string='Bank Account UPI')
    branch_id = fields.Many2one('bank.branch.lines', string='Branch', ondelete='cascade', index=True)


class BankBranchLines(models.Model):
    _name = "bank.branch.lines"
    _description = "Bank Branches"
    
    bank_id = fields.Many2one('res.bank', string='Bank', ondelete='cascade', index=True)
    name = fields.Char(string='Branch Name')
    bank_loc_zip = fields.Char(string='Branch Location ZIP')
    bank_ifsc = fields.Char(string='Branch IFSC')
    bank_imrc = fields.Char(string='Branch IMRC')
    bank_acc_upi = fields.Char(string='Bank Account UPI')

    
    
class AccountJournal(models.Model):
    _inherit = "account.journal"
    _description = "Journal Branch"
    
    branch_id = fields.Many2one('bank.branch.lines', string='Branch', ondelete='cascade', index=True)


        
        
        
    
    