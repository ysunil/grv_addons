# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import api, fields, models,_
from num2words import num2words


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    sgst_total=fields.Monetary('State GST [SGST]',readonly='True',compute='_amount_all', store=True)
    cgst_total=fields.Monetary('Central GST [CGST]',readonly='True',compute='_amount_all', store=True)
    igst_total=fields.Monetary('Integrated GST [IGST]',readonly='True',compute='_amount_all', store=True)
    
    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            amount_untaxed = amount_tax = sgst_total = cgst_total = igst_total =0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                for tax in line.tax_id:
                    if tax.amount_type=='group':
                        for tax_group_line in tax.children_tax_ids:
                            if tax_group_line.description=='CGST':
                                cgst_total+= (tax_group_line.amount*line.price_subtotal)/100
                            if tax_group_line.description=='SGST':
                                sgst_total+= (tax_group_line.amount*line.price_subtotal)/100
                    if tax.amount_type=='percent':
                        igst_total+= (tax.amount*line.price_subtotal)/100
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=order.partner_shipping_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
                'cgst_total': order.pricelist_id.currency_id.round(cgst_total),
                'sgst_total': order.pricelist_id.currency_id.round(sgst_total),
                'igst_total': order.pricelist_id.currency_id.round(igst_total),
            })

SaleOrder()


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

#    domain filter in tax_id based on partner and company state
    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        res= super(SaleOrderLine, self).product_id_change()
        active_tax_ids=self.tax_id.search([('active','=',True),('type_tax_use','=','sale')])
        tax_domain = [('active_tax', '=','True'),('id','in',active_tax_ids.ids)]
        partner_state=self.order_id.partner_id.state_id
        company_state=self.order_id.company_id.state_id
        if partner_state == company_state:
            tax_domain +=[('amount_type', '=','group')]
            if self.product_id:
                gst_tax_ids=self.product_id.taxes_id.filtered(lambda tax_id:tax_id.amount_type== 'group')
                self.tax_id=[(6,0,gst_tax_ids.ids)]

        elif partner_state != company_state:
            tax_domain +=[('amount_type', '!=','group')]
            if self.product_id:
                igst_tax_ids=self.product_id.taxes_id.filtered(lambda tax_id:tax_id.amount_type!= 'group')
                self.tax_id=[(6,0,igst_tax_ids.ids)]

        domain = {'tax_id':tax_domain}
        return {'domain': domain}