# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import models, fields, api, _
from odoo.osv import expression, osv
from datetime import datetime,date
import re
import csv
import io
from base64 import b64decode
import logging
import datetime
LOG = logging.getLogger("dicttoxml")
import binascii
import os
import string
import time
import xlwt
import base64
from xlwt import easyxf
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition

class Binary(http.Controller):
 @http.route('/opt/download', type='http', auth="public")
 @serialize_exception
 def download_document(self,model,field,id,filename=None, **kw):
    """ Download link for files stored as binary fields.
    :param str model: name of the model to fetch the binary from
    :param str field: binary field
    :param str id: id of the record from which to fetch the binary
    :param str filename: field holding the file's name, if any
    :returns: :class:`werkzeug.wrappers.Response`
    """
    cr, uid, context = request.cr, request.uid, request.context
    env = api.Environment(cr, 1, {})  
    out_brw=env['output'].browse(int(id))
    filecontent = base64.b64decode(out_brw.xls_output or '')
    if not filecontent:
        return request.not_found()
    else:
       if not filename:
           filename = '%s_%s' % (model.replace('.', '_'), id)
       return request.make_response(filecontent,
                      [('Content-Type', 'application/octet-stream'),
                       ('Content-Disposition', content_disposition(filename))])


class GSTR1(models.Model):
    
    _name = "gstr.one"
    _rec_name ='name'
    
    name=fields.Char(string="Name")
    periods_id=fields.Many2one('fiscal.year.period', string="Period")
    start_period=fields.Date(string="Start Of Period")
    end_period=fields.Date(string="End Of Period")
    state = fields.Selection([
            ('fetch_invoice','Fetch Invoice'),
            ('not_upload','Not Uploaded'),
            ('ready_to_upload', 'Ready To Upload'),
            ('upload_to_gov', 'Upload To Gov.'),
            ('failed', 'Failed'),
        ], string='Status', index=True, readonly=True, default='fetch_invoice', copy=False,)
    gstr1_bill_details_id=fields.One2many('gstr.one.bill.details','gstr1_id')
    company_id= fields.Many2one('res.company', 'Company', required=True)
    hsn_code=fields.Many2one('product.hsn.code','HSN Code')
    
    @api.onchange('periods_id')
    def _onchange_periods_id(self):
        if self.periods_id.start_period:
            self.start_period=self.periods_id.start_period
        else:
            self.start_period=False
        if self.periods_id.end_period:
            self.end_period=self.periods_id.end_period
        else:
            self.end_period=False
    
    @api.multi
    def fetch_invoice(self):
        inv_obj = self.env['account.invoice']
        gstr1_bill_details_obj = self.env['gstr.one.bill.details']
        start_period=self.start_period or False
        print("start_periodstart_periodstart_period",start_period)
        end_period=self.end_period or False
        inv_list=[]
        inv_ids=inv_obj.search([('date_invoice','>=',self.start_period),('date_invoice','<=',end_period),('state','in',('open','paid')),('type','in',('out_invoice','out_refund')),('company_id','=',self.company_id.id)])
        # print('-------------',inv_ids, inv_ids.partner_id.inv_type,inv_ids.amount_total)

        if inv_ids:
            for inv_id in inv_ids:
                if inv_id.partner_id.inv_type=='b2b':
                    inv_type=inv_id.partner_id.inv_type
                elif inv_id.partner_id.inv_type=='b2cl' and inv_id.amount_total>250000:
                    inv_type=inv_id.partner_id.inv_type
                elif inv_id.partner_id.inv_type=='b2cs' and inv_id.amount_total < 250000:
                    inv_type=inv_id.partner_id.inv_type
                else:
                    inv_type = inv_id.partner_id.inv_type
                vals={
                        'number': inv_id.number or False,
                        'invoice_id': inv_id.id,
                        'partner_id' : inv_id.partner_id.id or False,
                        'gstr1_id': self.id or False,
                        'amount_total': inv_id.amount_total or False,
                        'invoice_date': inv_id.date_invoice or False,
                        'invoice_status': inv_id.state or False,
                        'type': inv_id.type or False,
                        'gst_status': 'not_upload',
                        'inv_type': inv_type or False,
                     }
                gstr1_bill_details_obj.create(vals)
            self.state="not_upload"    
    
    @api.multi
    def generate_b2b_csv_file(self):
#        print'=================='
        cr= self.env.cr
        tax_obj = self.env['account.tax']
        for rec in self:
            if rec.gstr1_bill_details_id:
                for gstr_line in rec.gstr1_bill_details_id:
                    if gstr_line.inv_type=="b2b":
                        output = io.StringIO()
                        writer = csv.writer(output, quoting=csv.QUOTE_ALL)
                        Hedaer_Text = 'Outword Return'
                        writer.writerow(['GSTTIN/UIN of Recipient', 'Invoice Number','Invoice Date','Invoice Value','Place Of Supply','Reverse Charge','Invoice Type','E-commerce GSTIN.','Rate','Taxable value','Cess Amount'])
                        if gstr_line.invoice_id.tax_line_ids:
                            for inv_tax in gstr_line.invoice_id.tax_line_ids:
                                # tax_id=tax_obj.search([('name','=',inv_tax.name)])
                                writer.writerow([str(gstr_line.invoice_id.partner_id.vat), str(gstr_line.number),str(gstr_line.invoice_date),str(gstr_line.amount_total),str(gstr_line.invoice_id.partner_id.state_id.name),'No','Regular','',str(inv_tax.tax_id.amount),''])
                        cr.execute(""" DELETE FROM output""")
                        values = output.getvalue()
                        attach_id = self.env['output'].create({'name':Hedaer_Text+'.csv', 'xls_output': base64.b64encode(values.encode('utf-8'))})
                        print ("attach_idattach_idattach_idattach_id",attach_id)
                        return {
                             'type' : 'ir.actions.act_url',
                             'url': '/opt/download?model=output&field=xls_output&id=%s&filename=B2B Sale Return.csv'%(attach_id.id),
                             'target': 'self',
                            } 
                            
    @api.multi
    def generate_b2cs_csv_file(self):
        print("selffffffffffffffffff", self)
        cr= self.env.cr
        tax_obj = self.env['account.tax']
        for rec in self:
            if rec.gstr1_bill_details_id:
                for gstr_line in rec.gstr1_bill_details_id:
                    if gstr_line.inv_type=="b2cs":
                        output = io.StringIO()
                        writer = csv.writer(output, quoting=csv.QUOTE_ALL)
                        Hedaer_Text = 'Outword Return'
                        writer.writerow(['Invoice Number','Invoice Date','Invoice Value','Place Of Supply','Rate','Taxable value','Cess Amount'])
                        if gstr_line.invoice_id.tax_line_ids:
                            for inv_tax in gstr_line.invoice_id.tax_line_ids:
                                # tax_id=tax_obj.search([('name','=',inv_tax.name)])
                                writer.writerow([str(gstr_line.invoice_id.partner_id.vat), str(gstr_line.number),str(gstr_line.invoice_date),str(gstr_line.amount_total),str(gstr_line.invoice_id.partner_id.state_id.name),'No','Regular','',str(inv_tax.tax_id.amount),''])
                        cr.execute(""" DELETE FROM output""")
                        values = output.getvalue()
                        attach_id = self.env['output'].create({'name':Hedaer_Text+'.csv', 'xls_output': base64.b64encode(values.encode('utf-8'))})
#                        print ("attach_idattach_idattach_idattach_id",attach_id)
                        return {
                             'type' : 'ir.actions.act_url',
                             'url': '/opt/download?model=output&field=xls_output&id=%s&filename=B2CS Sale Return.csv'%(attach_id.id),
                             'target': 'self',
                            }

    @api.multi
    def generate_b2cl_csv_file(self):
        print("selffffffffffffffffff", self)
        cr = self.env.cr
        tax_obj = self.env['account.tax']
        for rec in self:
            if rec.gstr1_bill_details_id:
                for gstr_line in rec.gstr1_bill_details_id:
                    if gstr_line.inv_type == "b2cl":
                        output = io.StringIO()
                        writer = csv.writer(output, quoting=csv.QUOTE_ALL)
                        Hedaer_Text = 'Outword Return'
                        print("Hedaer_TextHedaer_TextHedaer_TextHedaer_TextHedaer_Text", Hedaer_Text)
                        writer.writerow(['Customer GST','Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Rate', 'Taxable value', 'Cess Amount'])
                        if gstr_line.invoice_id.tax_line_ids:
                            print("gstr_line.invoice_id.tax_line_ids",gstr_line.invoice_id.tax_line_ids,gstr_line.invoice_id.tax_line_ids.tax_id)
                            for inv_tax in gstr_line.invoice_id.tax_line_ids:
                                # tax_id = tax_obj.search([('name', '=', inv_tax.name)])
                                # print("tax_idtax_idtax_id", tax_id)
                                writer.writerow([str(gstr_line.invoice_id.partner_id.vat), str(gstr_line.number),
                                                 str(gstr_line.invoice_date), str(gstr_line.amount_total),
                                                 str(gstr_line.invoice_id.partner_id.state_id.name), 'No',
                                                 str(inv_tax.tax_id.amount), ''])
                        cr.execute(""" DELETE FROM output""")
                        print("output.getvalue()output.getvalue()",type(output.getvalue()))
                        values=output.getvalue()
                        print("valuesssssssssssssssssssssssssssssssssss", values)

                        attach_id = self.env['output'].create(
                            {'name': Hedaer_Text + '.csv', 'xls_output': base64.b64encode(values.encode('utf-8'))})
                        print ("attach_idattach_idattach_idattach_id",attach_id)

                        return {
                            'type': 'ir.actions.act_url',
                            'url': '/opt/download?model=output&field=xls_output&id=%s&filename=B2CL Sale Return.csv' % (attach_id.id),
                            'target': 'self',
                        }


    @api.multi
    def action_return(self):
        self.state='fetch_invoice'
        
    @api.multi
    def generate_csv_file(self):
        self.state='ready_to_upload'
        
    @api.multi
    def generate_hsn_csv_file(self):
        cr= self.env.cr
        output = io.BytesIO()
        if self._context.get('out_invoice')==True:
            inv_lines=self.env['account.invoice.line'].search([('product_id.l10n_in_hsn_code','=',self.hsn_code.name),('invoice_id.date_invoice','>=',self.start_period),('invoice_id.date_invoice','<=',self.end_period),('invoice_id.state','in',('open','done')),('invoice_id.type','=','out_invoice')])
        else:
            inv_lines=self.env['account.invoice.line'].search([('product_id.l10n_in_hsn_code','=',self.hsn_code.name),('invoice_id.date_invoice','>=',self.start_period),('invoice_id.date_invoice','<=',self.end_period),('invoice_id.state','in',('open','done')),('invoice_id.type','=','in_invoice')])
        if inv_lines:
            workbook = xlwt.Workbook()
            column_heading_style = easyxf('font:height 200;font:bold True;')
            column_heading_style_sum = easyxf('font:height 200;font:bold True;borders: top thin, bottom thin')
            worksheet = workbook.add_sheet('HSN Code Wise Summary')
            worksheet.write(2, 3, self.env.user.company_id.name, easyxf('font:height 200;font:bold True;align: horiz center;'))
            worksheet.write(3, 3, 'HSN CODE WISE REPORT', easyxf('font:height 200;font:bold True;align: horiz center;'))
            worksheet.write(4, 2, self.start_period, easyxf('font:height 200;font:bold True;align: horiz center;'))
            worksheet.write(4, 3, 'To',easyxf('font:height 200;font:bold True;align: horiz center;'))
            worksheet.write(4, 4, self.end_period,easyxf('font:height 200;font:bold True;align: horiz center;'))
            worksheet.write(6, 0, _('Product Name'), column_heading_style)
            worksheet.write(6, 1, _('Description'), column_heading_style)
            worksheet.write(6, 2, _('Unit of Measure'), column_heading_style)
            worksheet.write(6, 3, _('Quantity'), column_heading_style)
            worksheet.write(6, 4, _('Unit Price'), column_heading_style)
            worksheet.write(6, 5, _('Amount Untaxed'), column_heading_style)
            worksheet.write(6, 6, _('Tax Amount'), column_heading_style)
            worksheet.write(6, 7, _('CGST'), column_heading_style)
            worksheet.write(6, 8, _('SGST'), column_heading_style)
            worksheet.write(6, 9, _('Total Amount'), column_heading_style)
            counter=7
            total_unitprice=0.0
            total_untaxed=0.0
            total_quantity=0.0
            total_tax_amount=0.0
            total_amount=0.0
            total_cgst_sgst=0.0
            for line in inv_lines:
                tax_amount=0.0
                cgst_sgst=0.0
                worksheet.write(counter, 0, line.product_id.name)
                worksheet.write(counter, 1, line.name)
                worksheet.write(counter, 2, line.uom_id.name)
                worksheet.write(counter, 3, line.quantity)
                worksheet.write(counter, 4, line.price_unit)
                worksheet.write(counter, 5, line.price_subtotal)
                if line.invoice_line_tax_ids:
                    amount=(line.invoice_line_tax_ids[0].children_tax_ids[0].amount*2)/100
                    tax_amount=line.price_subtotal*amount
                    cgst_sgst=tax_amount/2
                worksheet.write(counter,6,tax_amount)
                worksheet.write(counter,7,cgst_sgst)
                worksheet.write(counter,8,cgst_sgst)
                worksheet.write(counter,9,line.price_subtotal+tax_amount)
                total_quantity+=line.quantity
                total_tax_amount+=tax_amount
                total_unitprice+=line.price_unit
                total_untaxed+=line.price_subtotal
                total_amount+=tax_amount+line.price_subtotal
                total_cgst_sgst+=cgst_sgst
                counter+=1
            worksheet.write(counter,3,total_quantity,column_heading_style_sum)
            worksheet.write(counter,4,total_unitprice,column_heading_style_sum)
            worksheet.write(counter,5,total_untaxed,column_heading_style_sum)
            worksheet.write(counter,6,total_tax_amount,column_heading_style_sum)
            worksheet.write(counter,7,total_cgst_sgst,column_heading_style_sum)
            worksheet.write(counter,8,total_cgst_sgst,column_heading_style_sum)
            worksheet.write(counter,9,total_amount,column_heading_style_sum)
            workbook.save(output)
            cr.execute(""" DELETE FROM output""")
            attach_id = self.env['output'].create(
                {'name': 'HSN Report' +'.xls', 'xls_output': base64.encodestring(output.getvalue())})
            return {
                'type': 'ir.actions.act_url',
                'url': '/opt/download?model=output&field=xls_output&id=%s&filename=HSN_Record.xls' % (attach_id.id),
                'target': 'self',
            }
        
class GSTR1BillDetails(models.Model):
    
    _name = "gstr.one.bill.details"
    _rec_name ='number'
    
    number=fields.Char(string="Number")
    invoice_id=fields.Many2one('account.invoice', string="Invoice")
    invoice_date=fields.Date("Bill Date")
    invoice_status=fields.Selection([
            ('paid','Paid'),
            ('open', 'Not Paid'),
        ], string='Bill Status', index=True, copy=False,)
    type=fields.Selection([
            ('out_invoice','Cutomer Invoice'),
            ('out_refund', 'Cutomer Invoice Return'),
        ], string='Type', index=True, copy=False,)
    inv_type=fields.Selection([
            ('b2b','B2B'),
            ('b2cl', 'B2CL'),
            ('b2cs', 'B2CS'),
        ], string='Type', index=True, copy=False,)
    gst_status=fields.Selection([
            ('uploaded','Uploaded'),
            ('not_upload', 'Not Uploaded'),
        ], string='GST Status', index=True, copy=False,)
    amount_total=fields.Float(string="Total")
    partner_id=fields.Many2one('res.partner', string="Partner")
    gstr1_id=fields.Many2one('gstr.one', string="Partner")

    # str(tax_id.amount),