# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning

class Partner(models.Model):
    _inherit = 'res.partner'
    
    # gst = fields.Char(string='GST No', size=15)
    pan_no=fields.Char('Pan No')
    inv_type=fields.Selection([
            ('b2b','B2B'),
            ('b2cl', 'B2CL'),
            ('b2cs', 'B2CS'),
        ], string='Type', index=True, copy=False,)
    
    @api.onchange('vat')
    def onchange_vat(self):
        state_obj=self.env['res.country.state']
        if self.vat:
            value=self.vat[:2]
            state_id=state_obj.search([('code','=',value),('country_id','=',self.company_id.country_id.id)])
            print("state_idstate_idstate_idstate_idstate_idstate_id", state_id, state_id.code)
            if int(state_id.code)<=36:
                print("stateeeeeeeeeeeee", state_id.code)
                self.state_id=state_id.id
            else:
                raise RedirectWarning(_('Please enter valid GST number'))
Partner()