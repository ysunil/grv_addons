# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import api, fields, models, _

class ProductHSNCode(models.Model):
    _name = 'product.hsn.code'
    
    name=fields.Char('Name', required=True)
    customer_tax_id = fields.Many2many('account.tax', 'hsn_customer_taxes_rel', 'hsn_id', 'tax_id', string='Customer Taxes',
        domain=[('type_tax_use', '=', 'sale'),('active_tax', '=', True)])
    supplier_tax_id = fields.Many2many('account.tax', 'hsn_supplier_taxes_rel', 'hsn_id', 'tax_id', string='Vendor Taxes',
        domain=[('type_tax_use', '=', 'purchase'),('active_tax', '=', True)])
    
ProductHSNCode()