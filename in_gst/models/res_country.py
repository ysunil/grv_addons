# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################
from odoo import models, fields


class ResCountryState(models.Model):
    _inherit = 'res.country.state'

    active = fields.Boolean(default=False,)
    
class ResCountry(models.Model):
    _inherit = 'res.country'

    active = fields.Boolean(default=False,)
