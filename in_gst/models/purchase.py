# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import models, fields, api,_
from num2words import num2words

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    
    sgst_total=fields.Monetary('State GST [SGST]',readonly='True',compute='_amount_all', store=True)
    cgst_total=fields.Monetary('Central GST [CGST]',readonly='True',compute='_amount_all', store=True)
    igst_total=fields.Monetary('Integrated GST [IGST]',readonly='True',compute='_amount_all', store=True)
    
    @api.depends('order_line.price_total')
    def _amount_all(self):
        for order in self:
            amount_untaxed = amount_tax = sgst_total = cgst_total = igst_total =0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                for tax in line.taxes_id:
                    if tax.amount_type=='group':
                        for tax_group_line in tax.children_tax_ids:
                            if tax_group_line.description=='CGST':
                                cgst_total+= (tax_group_line.amount*line.price_subtotal)/100
                            if tax_group_line.description=='SGST':
                                sgst_total+= (tax_group_line.amount*line.price_subtotal)/100
                    if tax.amount_type=='percent':
                        igst_total+= (tax.amount*line.price_subtotal)/100
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
#                    print('00000line.price_tax',line.price_tax)
                    amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.currency_id.round(amount_untaxed),
                'amount_tax': order.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
                'cgst_total': order.currency_id.round(cgst_total),
                'sgst_total': order.currency_id.round(sgst_total),
                'igst_total': order.currency_id.round(igst_total),
            })
            
PurchaseOrder()

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.onchange('product_id')
    def onchange_product_id(self):
        res= super(PurchaseOrderLine, self).onchange_product_id()
        active_tax_obj=self.taxes_id.search([('active','=',True),('type_tax_use','=','purchase')])
        if self.order_id.partner_id.state_id.name == self.order_id.company_id.state_id.name:
            domain = {'taxes_id': [('amount_type', '=','group'),('active_tax', '=','True'),('id','=',[tax_id.id for tax_id in active_tax_obj]) ]}
            if self.product_id:
                for tax_id in self.product_id.supplier_taxes_id:
                    if tax_id.amount_type=='group':
                        res = {
                        'taxes_id': [(6, 0, tax_id.ids)],
                        'name': self.product_id.name,
                        }
                        self.update(res)
        if self.order_id.partner_id.state_id.name != self.order_id.company_id.state_id.name:
            domain = {'taxes_id': [('amount_type', '!=','group'),('active_tax', '=','True'),('id','=',[tax_id.id for tax_id in active_tax_obj]) ]}
            if self.product_id:
                for tax_id in self.product_id.supplier_taxes_id:
                    if tax_id.amount_type!='group':
                        res = {
                        'taxes_id': [(6, 0, tax_id.ids)],
                        'name': self.product_id.name,
                        }
                        self.update(res)
        return {'domain': domain}
PurchaseOrderLine()