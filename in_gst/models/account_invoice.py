# -*- encoding: utf-8 -*-
# Copyright Knacktechs SA

from odoo import api, fields, models, _
from num2words import num2words

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    sgst_total=fields.Monetary('State GST [SGST]',readonly='True',compute='_compute_amount', store=True)
    cgst_total=fields.Monetary('Central GST [CGST]',readonly='True',compute='_compute_amount', store=True)
    igst_total=fields.Monetary('Integrated GST [IGST]',readonly='True',compute='_compute_amount', store=True)
    
    
    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        for inv_order in self:
            for line in inv_order.invoice_line_ids:
                for tax in line.invoice_line_tax_ids:
                    if tax.amount_type=='group':
                        for tax_group_line in tax.children_tax_ids:
                            if tax_group_line.description=='CGST':
                                self.cgst_total+= (tax_group_line.amount*line.price_subtotal)/100
                            if tax_group_line.description=='SGST':
                                self.sgst_total+= (tax_group_line.amount*line.price_subtotal)/100
                    if tax.amount_type=='percent':
                        self.igst_total+= (tax.amount*line.price_subtotal)/100
        
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_tax = sum(line.amount for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign

#to convert amount in wrds
    def get_num2words_amount(self, amount):
        amt_word = num2words(amount, lang='en_IN')
        amt_word = str(amt_word).title()
        return amt_word + ' Only'
    
    @api.multi
    def invoice_print(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        assert len(self) == 1 , 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'indian_gst.report_invoice')
    
    
    
AccountInvoice()

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
    
    @api.onchange('product_id')
    def _onchange_product_id(self):
        res= super(AccountInvoiceLine, self)._onchange_product_id()
        active_tax_ids=self.invoice_line_tax_ids.search([('active','=',True)])
        tax_domain = [('active_tax', '=','True'),('id','in',active_tax_ids.ids)]
        partner_state=self.invoice_id.partner_id.state_id
        company_state=self.invoice_id.company_id.state_id
        if partner_state == company_state:
            tax_domain +=[('amount_type', '=','group')]
            if self.product_id:
                gst_tax_ids=self.product_id.taxes_id.filtered(lambda tax_id:tax_id.amount_type == 'group')
                self.invoice_line_tax_ids=[(6,0,gst_tax_ids.ids)]

        elif partner_state != company_state:
            tax_domain +=[('amount_type', '!=','group')]
            if self.product_id:
                igst_tax_ids=self.product_id.taxes_id.filtered(lambda tax_id:tax_id.amount_type!= 'group')
                self.invoice_line_tax_ids=[(6,0,igst_tax_ids.ids)]

        domain = {'invoice_line_tax_ids':tax_domain}
        return {'domain': domain}
AccountInvoiceLine()
