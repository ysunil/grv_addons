from openerp import api, fields, models
import datetime

class order(models.Model):

    _inherit = "pos.order"

    source = fields.Char('Source Document', readonly=1)
    is_refund = fields.Boolean('Is Refund', readonly=1)
    is_refunded = fields.Boolean('Refuned', readonly=1)
    parent_id = fields.Many2one('pos.order', 'Refund from', readonly=1)
    child_ids = fields.One2many('pos.order', 'parent_id', 'Child')
    refund_date = fields.Datetime('Refund date')

    @api.model
    def create(self, vals):
        if 'parent_id'in vals:
            orders = self.search([('id', '=', vals['parent_id'])])
            orders.write({
                'is_refunded': True,
                'refund_date': datetime.datetime.now(),
            })
        return super(order, self).create(vals)

    @api.model
    def _order_fields(self, ui_order): #v10
        vals = super(order, self)._order_fields(ui_order)
        if 'is_refund' in ui_order:
            vals['is_refund'] = True
            vals['source'] = ui_order['source']
            vals['parent_id'] = ui_order['parent_id']
        return vals

class line(models.Model):

    _inherit = "pos.order.line"

    pos_reference = fields.Char(related="order_id.pos_reference", readonly=True, store=True)
    is_refund = fields.Boolean('Is refund')

class config(models.Model):

    _inherit = "pos.config"

    barcode_order = fields.Char('BarCode Order', size=64)
    allow_refund = fields.Boolean('Allow refund' ,default=False)
