{
    'name': 'POS Refund the orders',
    'sequence': 0,
    'version': '1.4',
    'author': 'TL Technology',
    'category': 'Point of Sale',
    'depends': ['pos_base'],
    'data': [
        'template/template.xml',
        'view/journal.xml',
        'view/pos.xml',
    ],
    'qweb': ['static/src/xml/*.xml'],
    'installable': True,
    'application': True,
    'price': '0',
    'website': 'http://bruce-nguyen.com',
    "currency": 'EUR',
    'images': ['static/description/icon.png'],
    'license': 'LGPL-3',
    'support': 'thanhchatvn@gmail.com'
}
