odoo.define('pos_refund_order', function(require) {
    "use strict"
    var core = require('web.core');
    var screens = require('point_of_sale.screens');
    var ScreenWidget = screens.ScreenWidget;
    var gui = require('point_of_sale.gui');
    var QWeb = core.qweb;
    var models = require('point_of_sale.models');
    var db = require('point_of_sale.DB');

    db.include({
        init: function(options) {
            this._super(options);
            this.order_by_uid = {};
            this.order_by_id = {};
            this.order_search_string = "";
            this.orders_sorted = [];
            this.line_by_pos_reference = {};
            this.line_by_id = {};
            this.order_by_code_reference = {};
        },
        _order_search_string: function(order){
            var str = order.id + ':' + ':' + order.pos_reference + ':' + order.name;
            return str + '\n';
        },
        add_orders: function(orders) {
            var updated_count = 0;
            if(!orders instanceof Array){
                orders = [orders];
            }
            for(var i = 0, len = orders.length; i < len; i++){
                var order = orders[i];
                this.order_by_code_reference[order.pos_reference] = order;
                this.orders_sorted.push(order);
                this.order_by_id[order.id] = order;
                this.order_search_string += this._order_search_string(order);
                updated_count += 1;
            }
            return updated_count;
        },
        get_order_by_code_reference: function(code) {
            var order = this.order_by_code_reference[code]
            return order;
        },
        get_all_order: function() {
            return this.order_by_id;
        },
        get_order_by_partner_id: function(partner_id) {
            var orders = this.get_all_order();
            var order_by_partner_id = [];
            if (!partner_id) {
                Object.keys(orders).forEach(function(order_id) {
                     order_by_partner_id.push(orders[order_id]);
                });
            }
            Object.keys(orders).forEach(function(order_id) {
                if (orders[order_id].partner_id[0] === partner_id) {
                    order_by_partner_id.push(orders[order_id]);
                }
            });
            return order_by_partner_id;
        },
        get_orders_sorted: function() {
            return this.orders_sorted;
        },
        reload_order_by_id: function(order) {
            if (!order) {
                return false;
            }
            if(!order instanceof Array){
                order = [order];
            }
            for(var i = 0, len = order.length; i < len; i++){
                var ord = order[i];
                this.order_by_id[ord.id] = ord;
            }
            return true;
        },
        remove_order_by_pos_reference: function(code) {
            var orders_before = this.orders_sorted;
            var orders_after = _.filter(orders_before, function(order){
                return order.pos_reference !== code;
            });
            this.orders_sorted = orders_after;
            },
        remove_order_line_by_pos_reference: function(code) {
            if (code) {
                delete this.line_by_pos_reference[code]
            }
        },
        add_order_lines: function(lines) {
            if (!lines instanceof Array) {
                lines = [lines];
            }
            var count_up = 0
            for(var i = 0, len = lines.length; i < len; i++){
                var line = lines[i];
                this.line_by_id[line.id] = line;
                count_up += 1
                if (!this.line_by_pos_reference[line.pos_reference]) {
                    this.line_by_pos_reference[line.pos_reference] = []
                    this.line_by_pos_reference[line.pos_reference].push(line);
                } else {
                    this.line_by_pos_reference[line.pos_reference].push(line);
                }
            }
            console.log('add new line: ' + count_up)
        },
        /* return Lines the same pos_reference , this field inherit pos.order */
        get_line_by_pos_reference: function(pos_reference) {
            if (!pos_reference) {
                return [];
            }
            return this.line_by_pos_reference[pos_reference];
        },
        get_line_by_id: function(line_id) {
            return this.line_by_id[line_id];
        },
        reload_order_lines_by_order: function(lines, order_id) {
            if (!lines) return false;
            if (!lines instanceof Array) {
                lines = [lines];
            }
            this.line_by_order[order_id] = [];
            for(var i = 0, len = lines.length; i < len; i++){
                var line = lines[i];
                this.line_by_order[order_id].push(line);
                this.line_by_id[line.id] = line;
            }
            return true;
        },
        get_all_line: function() {
            return this.line_by_order;
        },
        /* Search order Recover Screen */
        search_order: function(query) {
            try {
                query = query.replace(/[\[\]\(\)\+\*\?\.\-\!\&\^\$\|\~\_\{\}\:\,\\\/]/g,'.');
                query = query.replace(' ','.+');
                var re = RegExp("([0-9]+):.*?"+query,"gi");
            } catch(e) {
                return [];
            }
            var results = [];
            for(var i = 0; i < this.limit; i++){
                var query = this.order_search_string
                var r = re.exec(this.order_search_string);
                if(r && r[1]){
                    var id = Number(r[1]);
                    if (this.order_by_id[id] !== undefined) {
                        results.push(this.order_by_id[id]);
                    } else {
                        var code = r
                        
                    }
                } else {
                    break;
                }
            }
            return results;
        },
    })
    var _super_order = models.Order;
    models.Order = models.Order.extend({
        export_as_JSON: function() {
            var json = _super_order.prototype.export_as_JSON.apply(this, arguments);
			if (this.is_refund) {
				json.is_refund = true;
				json.source = this.source;
				json.parent_id = this.parent_id
			}
            return json;
        },
    });
	models.load_models([
		{
			model:  'pos.order',
            fields: ['id', 'pos_reference', 'name', 'date_order', 'session_id', 'amount_total', 'partner_id', ],
            domain:  function(self){ return [['state', '!=', 'draft'], ['state', '!=', 'cancel'], ['is_refunded', '=', false], ['is_refund', '=', false]]; },
            loaded: function(self, orders){
				_.each(orders, function(order) {
					if (order.pos_reference && order.pos_reference.length > 1) {
						order['pos_reference'] = order.pos_reference
					}
				})
            	self.orders = orders;
                self.db.add_orders(orders);
            },
        },{
        	model: 'pos.order.line',
        	fields: ['name','mrp_product', 'discount', 'voucher_id', 'price_subtotal_incl', 'pos_reference', 'product_id', 'qty', 'price_unit', 'tax_ids', 'order_id'],
        	domain: function(self) {
        		var orders = self.orders;
        		var pos_references = []
        		for (var i = 0, len = orders.length; i < len; i++) {
        			pos_references.push(orders[i].pos_reference);
        		}
        		return [['pos_reference', 'in', pos_references]];
        	},
        	loaded: function(self, lines) {
        		self.pos_order_line = lines;
        		self.db.add_order_lines(lines);
        	}
        }, 
		
	]);
    var _super_orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function(){
            _super_orderline.initialize.apply(this, arguments);
            this.is_refund = false;
            this.refund_date = null;
        },

        export_as_JSON: function(){
            var json = _super_orderline.export_as_JSON.apply(this, arguments);
            if (this.is_refund) {
                json.is_refund = this.is_refund;
            }
            if (this.refund_date) {
                json.refund_date = this.refund_date;
            }
            return json;
        }
    });

    var RefundScreenWidget = ScreenWidget.extend({

        template: "RefundScreenWidget",

        init: function(parent, options){
            this._super(parent, options);
            this.partner_cache = new screens.DomCache();
        },
        auto_back: true,
        start: function () {
            var self = this;
            this.$('.made_refund_order').click(function(){
                self.made_refund_order();
            });
        },
        show: function(){
            var self = this;
            this._super();
            var e2key = function(e) {
                console.log(e.which)
                if (!e) return '';
                if (e.which == 82 && self.refund) {
                    self.made_refund_order();
                };
                if (e.which == 81) {
                    self.gui.show_screen('products');
                }
                if (e.which == 83) {
                    $('.searchbox >input').focus()
                }
            };

            var page5Key = function(e, customKey) {
                if (e) e.preventDefault();
                switch(e2key(customKey || e)) {
                    case 'left': /*...*/ break;
                    case 'right': /*...*/ break;
                }
            };

            $(document).bind('keyup', page5Key);
            $(document).trigger('keyup', [{preventDefault:function(){},keyCode:37}]); 

            var orders = this.pos.db.get_orders_sorted();
            this.render_list(orders);
            var search_timeout = null;
            this.$('.order-list-contents').delegate('.client-line','click',function(event){
                var element =  $(this);
                var code = element.data('code')
                self.line_select(event, element, code);
            });
            this.$('.searchbox input')[0].value = '';
            this.$('.back').click(function(){
                self.gui.back();
            });
            this.orders = []
            this.el.querySelector('.searchbox input').addEventListener('keyup',function (event) {
                clearTimeout(search_timeout);
                var query = this.value;
                search_timeout = setTimeout(function(){
                    self.perform_search(query,event.which === 13);
                },70);
            });
            this.pos.barcode_reader.set_action_callback({
                'order': self.barcode_order_action ? function(code){ self.barcode_order_action(code); } : undefined ,
            });
            var order = this.order;
        },
        barcode_order_action: function(code){
            var orders = this.pos.orders;
            for(var i = 0, len = orders.length; i < len; i++){
             var db_code = orders[i].pos_reference
                if(db_code === code.code){
                    this.add_recover_order(orders[i])
                }
            }
        },
        render_list: function(orders){
            var contents = this.$el[0].querySelector('.order-list-contents');
            contents.innerHTML = "";
            for(var i = 0, len = Math.min(orders.length,1000); i < len; i++){
                var order    = orders[i];
                var clientline_html = QWeb.render('RecoverOrders',{widget: this, order: order});
                var clientline = document.createElement('tbody');
                clientline.innerHTML = clientline_html;
                clientline = clientline.childNodes[1];
                contents.appendChild(clientline);
            }
        },
        perform_search: function(query, associate_result){
            var orders = null;
            if(query){
                var orders = this.pos.db.search_order(query);
                if ( associate_result && orders.length === 1){
                    this.new_order = orders[0];
                    this.pos_widget.screen_selector.back();
                }
            }else{
                var orders = this.pos.db.get_orders_sorted();
            }
            this.render_list(orders);
        },
        made_refund_order: function() {
            var self = this;
            var lines = this.pos.db.get_line_by_pos_reference(this.order_selected.pos_reference);
            var cache_order = this.pos.db.get_order_by_code_reference(this.order_selected.pos_reference);
            var uid = cache_order. pos_reference.split(' ')[1];
            var orders = this.pos.get('orders').models;
            var old_order = orders.find(function (order) {
                return order.old_uid == uid;
            })
            if (old_order) {
                this.pos.set('selectedOrder', old_order);
                this.gui.show_screen('products');
                return;
            }
            else if (lines && !old_order) {
                var options = {
                    pos_session_id: this.pos.pos_session.id,
                    pos: this.pos,
                }
                var new_order = new models.Order({}, options);
                new_order['old_uid'] = uid
                for(var i = 0; i < lines.length; i++){
                    var line = lines[i];
                    var product = this.pos.db.get_product_by_id(line.product_id[0]);
                    new_order.add_product(product, {
                        price: - line.price_unit,
                        quantity: line.qty,
                        discount: line.discount,
                    });
                    var new_line = new_order.get_last_orderline();
                    new_line['is_refund'] = true;
                    new_line['refund_date'] = new Date().toLocaleTimeString();
                    if (!new_order['source']) {
                        new_order['source'] = line.name
                        new_order['parent_id'] = line.order_id[0]
                    }
                }
                if (self.order_selected.partner_id && self.order_selected.partner_id[0]) {
                    new_order.set_client(this.pos.db.get_partner_by_id(self.order_selected.partner_id[0]));
                }
                new_order['is_refund'] = true;
                new_order.trigger('change', new_order);
                this.pos.get('orders').add(new_order);
                this.pos.set('selectedOrder', new_order);
                this.gui.show_screen('products');
                return new_order;
            }
        },
        line_select: function(event, element, code){
            var order = this.pos.db.get_order_by_code_reference(code);
            var contents = this.$('.order-details-contents');
            contents.empty();
            this.order_selected = order;
            var all_element_actived = this.$('.highlight');
            var highlight =  element.hasClass('highlight');
            if (all_element_actived.length == 0){
                element.addClass('highlight');

            } else {
                this.$('.order-list-contents .highlight').removeClass('highlight');
                element.addClass('highlight');

            }
            contents.append($(QWeb.render('OrderInformationWidget',{ widget: this, order: order})));
            this.show_refund_button();
        },
        show_refund_button: function(){
            var $button = this.$('.made_refund_order');
            $button.removeClass('oe_hidden');
        },
        partner_icon_url: function(id){
            return '/web/image?model=res.partner&id='+id+'&field=image_small';
        },
    });
    gui.define_screen({name:'refund_screen', widget: RefundScreenWidget});

    var RefundButton = screens.ActionButtonWidget.extend({
        template: 'RefundButton',
        button_click: function () {
            this.gui.show_screen('refund_screen');
        },
    });
    screens.define_action_button({
        'name': 'refund_button',
        'widget': RefundButton,
        'condition': function () {
            return this.pos.config.allow_refund;
        },
    });
    return{
        RefundScreenWidget:RefundScreenWidget,
    }
})