# -*- coding: utf-8 -*-
from odoo import fields, models, api,_
from odoo.exceptions import UserError

class AccountBankStmtCashWizard(models.Model):
    _inherit = 'account.bank.statement.cashbox'

    @api.multi
    def get_value(self):
        cashbox_lines_ids_list,coin_value_list=[],[1,2,5,10,20,50,100,200,500,2000]
        for coin_vals in coin_value_list:
            vals=(0,0,{"coin_value":coin_vals})
            cashbox_lines_ids_list.append(vals)
        self.cashbox_lines_ids=cashbox_lines_ids_list
        return {
            "type": "set_scrollTop",
        }
class PosSession(models.Model):
    _inherit = 'pos.session'

    @api.multi
    def action_pos_session_closing_control(self):
    	statement_ids=self.statement_ids.filtered(lambda s: s.journal_id.pos_payment_reconcile == True)
    	diffrence_amount=0.0
    	if statement_ids:
	    	for statement_id in statement_ids:
	    		diffrence_amount+=statement_id.diff_amount
	    	if diffrence_amount!=0.0:
	    		raise UserError(_("Set First closing Balance Bank Payment"))
    	res=super(PosSession, self).action_pos_session_closing_control()
    	return res

