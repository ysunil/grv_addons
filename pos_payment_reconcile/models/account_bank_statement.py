# -*- coding: utf-8 -*-
from odoo import fields, models,api,_


class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    @api.depends('difference','diff_check')
    def compute_diff_amount(self):
    	for rec in self:
            rec.diff_amount=0.0
            if rec.diff_check==False:
                rec.diff_amount=rec.difference

    diff_amount = fields.Float('Difference', compute="compute_diff_amount")
    diff_check = fields.Boolean('Chek Difference', default=False)
    pos_payment_reconcile=fields.Boolean(related="journal_id.pos_payment_reconcile")
    @api.multi
    def action_pos_payment_reconcile(self):
        action = {
            'name': _('Cash Control'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.payment.reconcile',
            'view_id': self.env.ref('pos_payment_reconcile.pos_payment_reconcile_view_form').id,
            'type': 'ir.actions.act_window',
            'target': 'new'
        }
        return action
