# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    pos_payment_reconcile = fields.Boolean('POS Payment Reconcile', default=False)
