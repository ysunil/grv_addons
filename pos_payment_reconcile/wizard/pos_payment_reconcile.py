# -*- coding: utf-8 -*-
from odoo import models, api, fields,_
from odoo.exceptions import UserError

class PosPaymentReconcile(models.TransientModel):
    _name = 'pos.payment.reconcile'

    no_of_receipts = fields.Integer('No of Receipts')
    amount = fields.Float('Amount')
    comment = fields.Text('Comment')
    not_match = fields.Boolean()

    @api.multi
    def action_confirm(self):
        if self._context.get('active_id'):

            statement_id = self.env['account.bank.statement'].browse(self._context.get('active_id'))
            if statement_id:
                if statement_id.difference != -(self.amount) and self.comment == False:
                    self.not_match = True
                    return {
                        "type": "set_scrollTop",
                    }
                statement_id.diff_check = True
        # return {
        #     "type": "set_scrollTop",
        # }

    # @api.multi
    # def action_confirm(self):
    # 	if self._context.get('active_id'):
    #         statement_id=self.env['account.bank.statement'].browse(self._context.get('active_id'))
    #         if statement_id:
    #             if statement_id.difference!= -(self.amount):
    #                 raise UserError(_("Amount not matched"))
    #             statement_id.diff_check=True

