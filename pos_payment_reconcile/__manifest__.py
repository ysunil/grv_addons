# -*- coding: utf-8 -*-
{
    'name': 'POS Payment Reconcile',
    'version': '10.0.1.0.0',
    'summary': """""",
    'category': 'Point Of Sale',
    'license': 'LGPL-3',
    'author': "Knacktechs Solution",
    'website': "http://knacktechs.com/",
    'depends': ['point_of_sale'],
    'data': [
        'wizard/pos_payment_reconcile_view.xml',
        'views/account_journal_view.xml',
        'views/pos_session_view.xml',
    ],
    'images': [
        'static/description/app.png'
    ],
    'installable': True,
    'application': True,
    'qweb': [],
}
