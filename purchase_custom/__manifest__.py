# -*- coding: utf-8 -*-
{
    'name': 'Purchase Custom',
    'version': '10.0.1.0.0',
    'summary': """Purchase Custom""",
    'category': 'Purchase',
    'license': 'LGPL-3',
    'author': "Knacktechs Solution",
    'website': "http://knacktechs.com/",
    'depends': ['base','product','purchase','account'],
    'data': [
        'data/product_data.xml',
        'wizard/add_product.xml',
        'views/point_of_sale.xml',
        'views/res_partner_view.xml',
        'views/product_view.xml',
        'views/account_invoice_view.xml',
        'views/purchase_order_view.xml',

    ],
    'images': [
        'static/description/app.png'
    ],
    'qweb': ['static/src/xml/res_partner.xml',
        ],
    'installable': True,
    'application': True,
}
