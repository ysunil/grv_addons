# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ProductTemplate(models.Model):
    _inherit = "product.template"
    
    @api.multi
    def name_get(self):
        return [(template.id, '%s' % (template.name))
                for template in self]


    default_code = fields.Char(
        'Internal Reference', compute='_compute_default_code',
        inverse='_set_default_code', store=True,default='New', readonly=True)
    mrp_product=fields.Float('MRP Price')
    item_short_name=fields.Text('Short Name')
    purchase_gst_calc_on=fields.Text('PURCHASE GST CALC ON')
    sales_gst_calc_on=fields.Text('SALES GST CALC ON  ')

    @api.model
    def create(self, vals):
        if not vals.get('default_code')=="New":
            vals['default_code'] = self.env['ir.sequence'].next_by_code('product.template')
        result = super(ProductTemplate, self).create(vals)
        return result
    
class ProductProduct(models.Model):
    _inherit = "product.product"
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if args is None:
            args = []

        if self.env.context.get('search_default_seller_id'):
            supplier_args = args + [('seller_ids.name.id', '=', self.env.context['search_default_seller_id'])]
            products = super(ProductProduct, self).name_search(name, args=supplier_args, operator=operator, limit=limit)
            if products:
                return products

        return super(ProductProduct, self).name_search(name, args=args, operator=operator, limit=limit)

    @api.multi
    def name_get(self):
        # TDE: this could be cleaned a bit I think

        def _name_get(d):
            name = d.get('name', '')
#            code = self._context.get('display_default_code', True) and d.get('default_code', False) or False
#            if code:
#                name = '%s' % (name)
            return (d['id'], name)

        partner_id = self._context.get('partner_id')
        if partner_id:
            partner_ids = [partner_id, self.env['res.partner'].browse(partner_id).commercial_partner_id.id]
        else:
            partner_ids = []

        # all user don't have access to seller and partner
        # check access and use superuser
        self.check_access_rights("read")
        self.check_access_rule("read")

        result = []
        for product in self.sudo():
            # display only the attributes with multiple possible values on the template
            variable_attributes = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped('attribute_id')
            variant = product.attribute_value_ids._variant_name(variable_attributes)

            name = variant and "%s (%s)" % (product.name, variant) or product.name
            sellers = []
            if partner_ids:
                sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and (x.product_id == product)]
                if not sellers:
                    sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and not x.product_id]
            if sellers:
                for s in sellers:
                    seller_variant = s.product_name and (
                        variant and "%s (%s)" % (s.product_name, variant) or s.product_name
                        ) or False
                    mydict = {
                              'id': product.id,
                              'name': seller_variant or name,
#                              'default_code': s.product_code or product.default_code,
                              }
                    temp = _name_get(mydict)
                    if temp not in result:
                        result.append(temp)
            else:
                mydict = {
                          'id': product.id,
                          'name': name,
#                          'default_code': product.default_code,
                          }
                result.append(_name_get(mydict))
        return result
