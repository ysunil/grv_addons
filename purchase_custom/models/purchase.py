from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import ValidationError
from datetime import datetime
import datetime

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    
    expiry_date=fields.Date('Expiry Date')
    state = fields.Selection([
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),
        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ('expired', 'Expired')
        ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')
    weekly_amount=fields.Float('Weekly Amount')
    monthly_amount=fields.Float('Monthly Amount')



    @api.onchange('partner_id', 'company_id')
    def onchange_partner_id(self):
        rec = super(PurchaseOrder, self).onchange_partner_id()
        self.date_planned = datetime.datetime.now()
        return rec

    @api.depends('order_line.price_total','discount_type','discount_amt','discount_val')
    def _amount_all(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            if order.discount_type=='remove':
                order.discount_val=0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            amount_total=amount_untaxed + amount_tax
            if order.discount_type=='percent':
                amount_total =amount_total*(1 - (order.discount_val or 0.0) / 100.0)
            else:
                amount_total =amount_total-order.discount_val
            order.update({
                'amount_untaxed': order.currency_id.round(amount_untaxed),
                'amount_tax': order.currency_id.round(amount_tax),
                'amount_total': amount_total,
                'discount_amt': amount_untaxed+amount_tax-amount_total,
            })
            
    discount_type = fields.Selection([('remove','Remove'),('percent','Percent'),('amount','Amount')],'Discount By',)
    discount_val=fields.Float('Discount Value',digits=dp.get_precision('Discount'), default=0.0)
    discount_amt=fields.Float('Discount Amt',compute="_amount_all", digits=dp.get_precision('Discount'), default=0.0)
    discount_note=fields.Text('Discount note')

    @api.multi
    def button_add_product(self):
        serial_no = 1
        vendor_obj = self.env['product.supplierinfo']
        product_obj = self.env['product.product']
        sale_obj = self.env['sale.order.line']
        vendor_ids = vendor_obj.search([('name', '=',self.partner_id.id)])
        today = datetime.datetime.now().date()
        week_ago = today - datetime.timedelta(days=7)
        today_fifteen = datetime.datetime.now().date()
        fifteen_day_ago = today - datetime.timedelta(days=15)
        today_month = datetime.datetime.now().date()
        month_ago = today_month - datetime.timedelta(days=30)
        month_convert_into_time=datetime.datetime.strptime(str(month_ago), "%Y-%m-%d").strftime('%Y-%m-%d %H:%M:%S')
        week_convert_into_time=datetime.datetime.strptime(str(week_ago), "%Y-%m-%d").strftime('%Y-%m-%d %H:%M:%S')
        fifteen_day_ago=datetime.datetime.strptime(str(fifteen_day_ago), "%Y-%m-%d").strftime('%Y-%m-%d %H:%M:%S')
        product_list=[]
        if vendor_ids:
            for product in vendor_ids:
                price_with_tax,price_without_tax,line_id_list,final_list=0,0,[],[]
                price_seven,qty_seven,average_seven=0,0,0
                price_fifteen,qty_fifteen,average_fifteen=0,0,0
                price_thirty,qty_thirty,average_thirty=0,0,0
                product_ids=product_obj.search([('product_tmpl_id','=',product.product_tmpl_id.id)],limit=1)
                purchase_lines = self.env['purchase.order.line'].sudo().search([('partner_id', '=', self.partner_id.id),('product_id', '=', product_ids.id),('order_id.state','in',('purchase','done'))])
                if purchase_lines:
                    for lines in purchase_lines:
                        line_id_list.append(lines.id)
                final_list = sorted(line_id_list, key=int, reverse=True)
                if len(final_list)>=1:
                    price_without_tax = self.env['purchase.order.line'].sudo().browse(final_list[0])
                    tax = price_without_tax.price_tax/price_without_tax.product_qty
                    price_with_tax = price_without_tax.price_unit+tax
                    price_without_tax = price_without_tax.price_unit
                if self.order_line:
                    line_id=self.order_line.filtered(lambda x: x.product_id.id == product_ids.id)
                    if line_id:
                        continue
                sale_id_7 = sale_obj.search([('date_order_line', '>=',week_convert_into_time),('product_id', '=',product_ids.id)])
                sale_id_15 = sale_obj.search([('date_order_line', '>=',fifteen_day_ago),('product_id', '=',product_ids.id)])
                sale_id_30 = sale_obj.search([('date_order_line', '>=',month_convert_into_time),('product_id', '=',product_ids.id)])
                if sale_id_30:
                    for line in sale_id_30:
                        price_thirty+=line.price_unit
                        qty_thirty+=line.product_uom_qty
                    average_thirty=price_thirty/len(sale_id_30)
                if sale_id_7:
                    for line in sale_id_7:
                        price_seven+=line.price_unit
                        qty_seven+=line.product_uom_qty
                    average_seven=price_seven/len(sale_id_30)
                if sale_id_15:
                    for line in sale_id_15:
                        price_fifteen+=line.price_unit
                        qty_fifteen+=line.product_uom_qty
                    average_fifteen=price_fifteen/len(sale_id_30)
                vals = (0,0,{'product_id':product_ids.id,'ordered_quantity':1,'product_quantity':qty_seven,'sale_price':average_seven,'sold_quantity_30':qty_thirty,'sold_price_30':average_thirty,'sold_quantity_15':qty_fifteen,'sold_price_15':average_fifteen,'on_hand_quantity':product.product_tmpl_id.qty_available,
                    'product_internal_code':product.product_tmpl_id.default_code,'purchase_cost':product.product_tmpl_id.standard_price,'mrp':product.product_tmpl_id.mrp_product,'sr_no':serial_no,'last_purchase_price_without_gst':price_without_tax,'last_purchase_price_with_gst':price_with_tax})
                product_list.append(vals)
                serial_no+=1
      
        view_ref = self.env['ir.model.data'].get_object_reference('purchase_custom', 'view_inherit_wizard_purchase_order_form')
        view_id = view_ref[1] if view_ref else False
        return {
        'name': _('Message'),
        'view_type': 'form',
        'view_mode': 'form',
        'view_id': view_id,
        'res_model': 'add.product.wizard',
        'type': 'ir.actions.act_window',
        'target': 'new',
        'context':{'default_add_product_ids':product_list}
        } 



class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"
    
    #add discount calculation
    @api.depends('product_qty', 'price_unit','discount_by','discount_type', 'discount_val', 'discount_amt','taxes_id')
    def _compute_amount(self):
        for line in self:
            price=line.price_unit
            if not line.discount_type:
                line.discount_val=0.0
            if line.discount_by:
                if line.discount_type=='percent':
                    price =line.price_unit * (1 - (line.discount_val or 0.0) / 100.0)
                else:
                    price =line.price_unit - line.discount_val
                
            taxes = line.taxes_id.compute_all( price, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
            if line.discount_val>0.0:
                line.discount_amt=line.product_qty*line.price_unit-taxes['total_excluded']
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })
            
    discount_by = fields.Selection([('normal_disc','Normal Discount'),('scheme_discount','Scheme Discount')],'Discount By',)
    discount_type = fields.Selection([('percent','Percent'),('amount','Amount')],'Discount',)
    discount_val=fields.Float('Discount Value',digits=dp.get_precision('Discount'), default=0.0)
    discount_amt=fields.Float('Discount Amt',compute='_compute_amount',digits=dp.get_precision('Discount'), default=0.0)
    


class SaleOrderLineProduct(models.Model):
    _inherit = "sale.order.line"
    
    date_order_line = fields.Datetime(related="order_id.confirmation_date", string="Date", store=True)


