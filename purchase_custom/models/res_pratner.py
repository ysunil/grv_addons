from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning

class Partner(models.Model):
    _inherit = 'res.partner'
    
    customer_code = fields.Char(string='Customer Code', size=16)
    
    sql_constraints = [('customer_code_company_uniq', 'unique (customer_code,company_id)', 'The name of the customer code must be unique per company!')]   