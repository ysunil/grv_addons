from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.addons import decimal_precision as dp

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    #Total discount calculate 
    @api.one
    @api.depends('invoice_line_ids.price_subtotal','discount_type','discount_val', 'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
                 'currency_id','discount_amt', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        round_curr = self.currency_id.round
        if self.discount_type=='remove':
                self.discount_val=0.0
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_tax = sum(round_curr(line.amount_total) for line in self.tax_line_ids)
        amount_total = self.amount_untaxed + self.amount_tax
        if self.discount_type=='percent':
                amount_total =amount_total*(1 - (self.discount_val or 0.0) / 100.0)
        elif self.discount_type=='amount':
            amount_total =amount_total-self.discount_val
        self.amount_total=amount_total
        self.discount_amt=self.amount_untaxed + self.amount_tax-self.amount_total
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign
        
    #Return incoming picking Type
    @api.model
    def _default_picking_receive(self):
        type_obj = self.env['stock.picking.type']
        company_id = self.env.context.get('company_id') or self.env.user.company_id.id
        types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id.company_id', '=', company_id)])
        if not types:
            types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id', '=', False)])
        return types[:1]
    
    discount_type = fields.Selection([('remove','Remove'),('percent','Percent'),('amount','Amount')],'Discount By',)
    discount_val=fields.Float('Discount Value',digits=dp.get_precision('Discount'), default=0.0)
    discount_amt=fields.Float('Discount Amt', compute="_compute_amount", digits=dp.get_precision('Discount'), default=0.0)
    discount_note=fields.Text('Discount note')
    picking_count = fields.Integer(string="Count")
    invoice_picking_id = fields.Many2one('stock.picking', string="Picking Id")
    picking_type_id = fields.Many2one('stock.picking.type', 'Picking Type', required=True,
                                      default=_default_picking_receive,
                                      help="This will determine picking type of incoming shipment")

    @api.multi
    def action_stock_receive(self):
        for invoice in self:
            avilable_qty = any(inv_line.free_qty > 0.0 for inv_line in invoice.invoice_line_ids)
            if avilable_qty:
                print('test---------------')
                if not self.invoice_picking_id:
                    pick = {
                        'picking_type_id': invoice.picking_type_id.id,
                        'partner_id': invoice.partner_id.id,
                        'origin': invoice.number,
                        'location_dest_id': invoice.picking_type_id.default_location_dest_id.id,
                        'location_id': invoice.partner_id.property_stock_supplier.id,
                        'company_id': invoice.company_id.id,
                    }
                    picking = self.env['stock.picking'].create(pick)
                    invoice.invoice_picking_id = picking.id
                    invoice.picking_count = len(picking)
                    moves = invoice.invoice_line_ids.filtered(lambda r: r.product_id.type in ['product', 'consu'])._create_stock_moves(picking)
                    move_ids = moves._action_confirm()
                    move_ids._action_confirm()
                    picking.action_done()
                    picking.button_validate()
            
    @api.multi
    def action_invoice_open(self):
        res=super(AccountInvoice,self).action_invoice_open()
        if self.type in ('in_invoice'):
            self.action_stock_receive()
        return res
    
class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"
    
    @api.one
    @api.depends('price_unit','discount_by','discount_type','discount_val', 'discount_amt', 'invoice_line_tax_ids', 'quantity',
        'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
        'invoice_id.date_invoice')
    def _compute_price(self):
        currency = self.invoice_id and self.invoice_id.currency_id or None
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        if not self.discount_type:
            self.discount_val=0.0
        if self.discount_by:
            print('discount_bydiscount_by---')
            if self.discount_type=='percent':
                print('pricepercentpercent',self.discount_type)
                price =self.price_unit * (1 - (self.discount_val or 0.0) / 100.0)
            else:
                price =self.price_unit - self.discount_val
                print()
            print('price===',price)
        taxes = False
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
        self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price
        if self.discount_val>0.0:
                self.discount_amt=(self.quantity * self.price_unit)-self.price_subtotal
        self.price_total = taxes['total_included'] if taxes else self.price_subtotal
        if self.invoice_id.currency_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice).compute(price_subtotal_signed, self.invoice_id.company_id.currency_id)
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        self.price_subtotal_signed = price_subtotal_signed * sign
        
    free_qty=fields.Float('Free Qty')
    discount_by = fields.Selection([('normal_disc','Normal Discount'),('scheme_discount','Scheme Discount')],'Discount By',store=True)
    discount_type = fields.Selection([('percent','Percent'),('amount','Amount')],'Discount',)
    discount_val=fields.Float('Discount Value',digits=dp.get_precision('Discount'), default=0.0)
    discount_amt=fields.Float('Discount Amt',compute='_compute_price',digits=dp.get_precision('Discount'), default=0.0)
    
    @api.multi
    def _create_stock_moves(self, picking):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        for line in self:
            price_unit = line.price_unit
            template = {
                'name': line.name or '',
                'product_id': line.product_id.id,
                'product_uom': line.uom_id.id,
                'origin': line.invoice_id.number,
                'location_id': line.invoice_id.partner_id.property_stock_supplier.id,
                'location_dest_id': picking.picking_type_id.default_location_dest_id.id,
                'picking_id': picking.id,
                'move_dest_id': False,
                'quantity_done': line.free_qty,
                'state': 'draft',
                'company_id': line.invoice_id.company_id.id,
                'price_unit': line.currency_id.compute(price_unit, line.invoice_id.company_id.currency_id),
                'picking_type_id': picking.picking_type_id.id,
                'procurement_id': False,
                'route_ids': 1 and [
                    (6, 0, [x.id for x in self.env['stock.location.route'].search([('id', 'in', (2, 3))])])] or [],
                'warehouse_id': picking.picking_type_id.warehouse_id.id,
            }
            diff_quantity = line.free_qty
            tmp = template.copy()
            tmp.update({
                'product_uom_qty': diff_quantity,
            })
            template['product_uom_qty'] = diff_quantity
            done += moves.create(template)
        return done
                