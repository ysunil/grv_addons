# -*- coding: utf-8 -*-

from odoo import api, fields, models
import datetime


class AddProduct(models.TransientModel):
    _name = "add.product.wizard"

    add_product_ids = fields.One2many('product.history','add_product_wizard_id',string="Product")
    check_box_wizard = fields.Boolean(string="Select All")


    @api.onchange('check_box_wizard')
    def check_box_wizard_onchange(self):
        if self.check_box_wizard == True:
        	for line in self.add_product_ids:
        	    line.check_box = True
        else:
            for line in self.add_product_ids:
        	    line.check_box = False

    @api.multi
    def add_product_order_line(self):
        add_product = []
        active_id =  self._context.get('active_ids')
        order_obj = self.env['purchase.order']
        order_id = order_obj.browse(active_id)
        if self.add_product_ids:
            if self.check_box_wizard == True:
                for line in self.add_product_ids:
                    vals=({'product_id':line.product_id.id,'name':line.product_id.name,'date_planned':datetime.datetime.now().date(),'product_uom':line.product_id.uom_id,'product_qty':line.ordered_quantity,'price_unit':line.product_id.list_price})
                    add_product.append(vals)
            
            else:
                for line in self.add_product_ids:
                    if line.check_box == True:
                        vals=({'product_id':line.product_id.id,'name':line.product_id.name,'date_planned':datetime.datetime.now().date(),'product_uom':line.product_id.uom_id,'product_qty':line.ordered_quantity,'price_unit':line.product_id.list_price})
                        add_product.append(vals)
            order_id.order_line=add_product






class AddProductWizard(models.TransientModel):
    _name = "product.history"

    add_product_wizard_id = fields.Many2one('add.product.wizard')
    product_id = fields.Many2one('product.product', string="Product")
    product_quantity = fields.Float(string="7 days sales")
    sale_price = fields.Float(string="7 days price") 
    check_box = fields.Boolean(string="Select")
    ordered_quantity = fields.Float(string="Order Quantity", default='1.00')
    sold_quantity_30 = fields.Float(string="30 days sales")
    sold_price_30 = fields.Float(string="30 days price")
    sold_quantity_15 = fields.Float(string="15 days sales")
    sold_price_15 = fields.Float(string="15 days price")
    on_hand_quantity = fields.Float(string="On-hand Quantity")
    product_internal_code = fields.Char(string="Internal Code")
    purchase_cost = fields.Float(string="Purchase Cost")
    mrp = fields.Float(string="MRP") 
    sr_no = fields.Integer(string="Sr No")
    last_purchase_price_with_gst = fields.Float(string="Last Purchase price with GST")
    last_purchase_price_without_gst = fields.Float(string="Last Purchase price without GST")




