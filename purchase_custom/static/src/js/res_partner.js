odoo.define('purchase_custom.models', function (require) {
"use strict";

var models = require('point_of_sale.models');

models.load_fields("res.partner", "customer_code");
models.load_fields("product.product",["mrp_product","default_code"]);
return models;
});
