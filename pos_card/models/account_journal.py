# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    card_info_required = fields.Boolean('Card info required?', default=False)
