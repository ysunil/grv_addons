# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    code_number = fields.Char('Code Number')
    auth_code = fields.Char('Auth code')
    card_info_required = fields.Boolean(related='journal_id.card_info_required', readonly=True)
    bank_name = fields.Char('Bank Name')