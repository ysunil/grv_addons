# -*- coding: utf-8 -*-
from odoo import models

class PosOrder(models.Model):
    _inherit = "pos.order"

    def _payment_fields(self, ui_paymentline):
        res = super(PosOrder, self)._payment_fields(ui_paymentline)
        res.update({
            'bank_name': ui_paymentline.get('bank_name'),
            'code_number': ui_paymentline.get('code_number'),
            'auth_code': ui_paymentline.get('auth_code'),
        })
        return res

    def add_payment(self, data):
        statement_id = super(PosOrder, self).add_payment(data)
        StatementLine = self.env['account.bank.statement.line']
        statement_lines = StatementLine.search([
            ('statement_id', '=', statement_id),
            ('pos_statement_id', '=', self.id),
            ('journal_id', '=', data['journal']),
            ('amount', '=', data['amount'])
        ])
        for line in statement_lines:
            if line.journal_id.card_info_required :

                card_vals = {
                    'bank_name': data.get('bank_name'),
                    'code_number': data.get('code_number'),
                    'auth_code': data.get('auth_code'),
                }
                line.write(card_vals)
                break

        return statement_id
