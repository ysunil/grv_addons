# -*- coding: utf-8 -*-
{
    'name': 'POS Card',
    'version': '10.0.1.0.0',
    'summary': """Allow users to pay by card and to record details about the card paid
 directly from the user interface""",
    'category': 'Point Of Sale',
    'license': 'LGPL-3',
    'author': "Anil Yadav",
    'website': "http://knacktechs.com/",
    'depends': ['point_of_sale'],
    'data': [
        'views/pos_card.xml',
        'views/account_journal_view.xml',
        'views/pos_order_view.xml',
    ],
    'images': [
    ],
    'installable': True,
    'application': True,
    'qweb': ['static/src/xml/pos_card.xml'],
}
