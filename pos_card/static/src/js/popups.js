odoo.define('pos_card.popups', function (require) {
"use strict";

var PopupWidget = require('point_of_sale.popups');
var gui = require('point_of_sale.gui');

var CardInfoWidget = PopupWidget.extend({
    template: 'CardInfoWidget',
    show: function(options){
        options = options || {};
        this._super(options);
        this.renderElement();
        $('body').off('keypress', this.keyboard_handler);
        $('body').off('keydown', this.keyboard_keydown_handler);
        window.document.body.addEventListener('keypress',this.keyboard_handler);
        window.document.body.addEventListener('keydown',this.keyboard_keydown_handler);
        if(options.data){
            var data = options.data;
            this.$('input[name=bank_name]').val(data.bank_name);
            this.$('input[name=code_number]').val(data.code_number);
            this.$('input[name=auth_code]').val(data.auth_code);
        }
    },
    click_confirm: function(){
        var infos = {
            'bank_name' : this.$('input[name=bank_name]').val(),
            'code_number'  : this.$('input[name=code_number]').val(),
            'auth_code'  : this.$('input[name=auth_code]').val(),
        };
        var valid = true;
        if(this.options.validate_info){
            valid = this.options.validate_info.call(this, infos);
        }

        if(!valid) return;

        this.gui.close_popup();
        if( this.options.confirm ){
            this.options.confirm.call(this, infos);
        }
    },
});
gui.define_popup({name:'card-info-input', widget: CardInfoWidget});

return PopupWidget;
});