odoo.define('pos_card.screens', function (require) {
"use strict";

var screens = require('point_of_sale.screens');

screens.PaymentScreenWidget.include({
    show_popup_card_info: function(options) {
        var self = this;
        window.document.body.removeEventListener('keypress', self.keyboard_handler);
        window.document.body.removeEventListener('keydown', self.keyboard_keydown_handler);
        this.gui.show_popup('card-info-input',{
            data: options.data,
            validate_info: function(infos){
                this.$('input').removeClass('error');
                if(!infos.bank_name) {
                    this.$('input[name=bank_name]').addClass('error');
                    this.$('input[name=bank_name]').focus();
                    return false;
                }
                if(!infos.code_number) {
                    this.$('input[name=code_number]').addClass('error');
                    this.$('input[name=code_number]').focus();
                    return false;
                }
                if(!infos.auth_code) {
                    this.$('input[name=auth_code]').addClass('error');
                    this.$('input[name=auth_code]').focus();
                    return false;
                }
//                }
                return true;
            },
            confirm: function(infos){
                options.confirm.call(self, infos);
                self.reset_input();
                self.render_paymentlines();
                window.document.body.addEventListener('keypress', self.keyboard_handler);
                window.document.body.addEventListener('keydown', self.keyboard_keydown_handler);
            },
            cancel: function(){
                window.document.body.addEventListener('keypress', self.keyboard_handler);
                window.document.body.addEventListener('keydown', self.keyboard_keydown_handler);
            },
        });
    },
    click_paymentmethods: function(id) {
        var self = this;
        var cashregister = null;
        for ( var i = 0; i < this.pos.cashregisters.length; i++ ) {
            if ( this.pos.cashregisters[i].journal_id[0] === id ){
                cashregister = this.pos.cashregisters[i];
                break;
            }
        }
        if (cashregister.journal.card_info_required) {
            this.show_popup_card_info({
                confirm: function(infos) {
                    //merge infos to new paymentline
                    self.pos.get_order().add_paymentline_with_card(cashregister, infos);
                },
            });
        }
        else {
            this._super(id);
        }
    },

    click_numpad: function(button) {
        var paymentlines = this.pos.get_order().get_paymentlines();
        var open_paymentline = false;

        for (var i = 0; i < paymentlines.length; i++) {
            if (! paymentlines[i].paid) {
                open_paymentline = true;
            }
        }

        if (! open_paymentline) {
            var cashregister = null;
            for ( var i = 0; i < this.pos.cashregisters.length; i++ ) {
                if (!this.pos.cashregisters[i].journal.card_info_required){
                    cashregister = this.pos.cashregisters[i];
                    break;
                }
            }
            this.pos.get_order().add_paymentline(cashregister);
            this.render_paymentlines();
        }

        this.payment_input(button.data('action'));
    },

    click_card_info_paymentline: function(cid){
        var self = this;
        var lines = this.pos.get_order().get_paymentlines();
        for ( var i = 0; i < lines.length; i++ ) {
            if (lines[i].cid === cid) {
                this.show_popup_card_info({
                    data: lines[i],
                    confirm: function(infos) {
                        //merge infos to updated paymentline
                        self.pos.get_order().update_paymentline_with_card(lines[i], infos);
                    },
                });
                return;
            }
        }
    },

    render_paymentlines: function() {
        var self = this;
        this._super();
        var lines = this.$('.paymentlines-container table.paymentlines');
        lines.on('click','.card-info-button', function(){
            self.click_card_info_paymentline($(this).data('cid'));
        });
    }

});

});
